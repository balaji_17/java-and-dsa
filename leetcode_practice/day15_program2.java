class solution69{
	public static void main(String[] args){
		int arr[]=new int[]{2,3,1,7,3,5,2,1,34,5,6,79,8,7,6,5,5,4,3,5,6,8,8,6,5,6,7,7,77};
		int n=arr.length;

		Bubble_sort(arr,n);

		for(int i=0;i<n;i++){
			System.out.print(arr[i]+" ");
		}
		
	}

	public static int[] Bubble_sort(int [] arr,int n){
		
		for(int i=0;i<n;i++){

			for(int j=0;j<n-1;j++){
				if(arr[j]>arr[j+1]){
					int temp=arr[j];
					arr[j]=arr[j+1];
					arr[j+1]=temp;
				}

			}
		
		}
		return arr;
	}
}
