import java.util.HashSet;
import java.util.ArrayList;

class Solution {
    public int[] intersection(int[] nums1, int[] nums2) {
        HashSet<Integer> set = new HashSet<>();
        HashSet<Integer> intersectSet = new HashSet<>();

       
        for (int num : nums1) {
            set.add(num);
        }

        // Check for common elements in nums2 and add to intersectSet
        for (int num : nums2) {
            if (set.contains(num)) {
                intersectSet.add(num);
            }
        }

        // Convert the HashSet to an array
        int[] newarr = new int[intersectSet.size()];
        int index = 0;
        for (int num : intersectSet) {
            newarr[index++] = num;
        }

        return newarr;
    }
}

