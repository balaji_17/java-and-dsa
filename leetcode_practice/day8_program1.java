/*Code2: Element with left side smaller and right side greater
Company: Zoho, Amazon, OYO Rooms, Intuit
Platform: GFG
Description :
Given an unsorted array of size N. Find the first element in the array such that all
of its left elements are smaller and all right elements are greater than it.
Note: Left and right side elements can be equal to required elements. And extreme
elements cannot be required.

Example 1:
Input:
N = 4
A[] = {4, 2, 5, 7}
Output:
5
Explanation:
Elements on left of 5 are smaller than 5
and on right of it are greater than 5.

Example 2:
Input:
N = 3
A[] = {11, 9, 12}
Output:
-1

Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)

Constraints:

Java
3 <= N <= 106
1 <= A[i] <= 106 */


class Compute {
    public int findElement(int arr[], int n) {
        for (int i = 1; i < n - 1; i++) {
            int num = arr[i];
            int j=0;
            int k=n-1;
            int flag=0;
            int hlag=0;
            
            while(j<i){
                if(arr[j]>arr[i]){
                    flag=1;
                    break;
                }
                j++;
            }
            while(k>i){
                if(arr[k]<arr[i]){
                    hlag=1;
                    break;
                }
                k--;
            }
           
           if(flag==0 && hlag==0){
               return arr[i];
           }
        }
        return -1;
    }
}

