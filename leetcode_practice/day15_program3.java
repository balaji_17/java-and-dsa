class Solution {
    public void transpose(int n, int[][] a) {
        int row = a.length;
        int column = a[0].length;
        int[][] transpose = new int[column][row];

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                transpose[j][i] = a[i][j];
            }
        }

        // Copy the transposed matrix back to the original matrix
        for (int i = 0; i < column; i++) {
            for (int j = 0; j < row; j++) {
                a[i][j] = transpose[i][j];
            }
        }
    }
}

