class Solution {

    public static int smallestSubWithSum(int a[], int n, int x) {
        int minLength = Integer.MAX_VALUE;
        int currentSum = 0;
        int left = 0;
        int right = 0;
        
        while (right < n) {
            currentSum += a[right];
            right++;
            
            while (currentSum > x && left < right) {
                minLength = Math.min(minLength, right - left);
                currentSum -= a[left];
                left++;
            }
        }
        
        return (minLength == Integer.MAX_VALUE) ? 0 : minLength;
    }
}

