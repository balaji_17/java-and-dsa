import java.util.*;
class Solve {
    int[] printUnsorted(int[] arr, int n) {
        int newarr[] = new int[n];
        int newarr2[] = new int[2];

        for (int i = 0; i < n; i++) {
            newarr[i] = arr[i];
        }

        Arrays.sort(newarr);
        int ans1 = -1;
        int ans2 = -1;
        for (int i = 0; i < n; i++) {
            if (arr[i] != newarr[i]) {
                ans1 = i;
                newarr2[0] = i;
                break;
            }
        }

        for (int i = n - 1; i >= 0; i--) {
            if (arr[i] != newarr[i]) {
                ans2 = i;
                newarr2[1] = i;
                break;
            }
        }

        return newarr2;
    }
}
