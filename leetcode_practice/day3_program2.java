/*Code2: Leaders in an Array
Company : PayU, Adobe, Microsoft, Synopsys, Coditas, Hashedln, Betsol
Platform : GFG
Description:

Given an array A of positive integers. Your task is to find the leaders in the
array. An element of an array is a leader if it is greater than or equal to all the
elements to its right side. The rightmost element is always a leader.

Example 1:
Input:
n = 6
A[] = {16,17,4,3,5,2}
Output: 17 5 2
Explanation: The first leader is 17 as it is greater than all the elements to its
right.
Similarly, the next leader is 5. The right most element is always a leader so it is
also included.
Example 2:
Input:
n = 5
A[] = {1,2,3,4,0}
Output: 4 0
Explanation: 0 is the rightmost element and 4 is the only element which is
greater
than all the elements to its right.
Expected Time Complexity: O(n)
Expected Auxiliary Space: O(n)
Constraints:
1 <= n <= 107
0 <= Ai <= 107 */

class Solution{
    //Function to find the leaders in the array.
   static int[] leaders(int arr[], int n){
        
	  int newarr[]=new int[n];
	  int k=0;
	   for(int i=0;i<n-1;i++){
            for(int j=i+1;j<n;j++){
                
                if(arr[i]<=arr[j]){
                    break;
                }else{
                    newarr[k++]=arr[i];
                }
                
                
            }
        }
        return newarr;
    }
   public static void main(String[] args){
	int arr[]=new int[]{16,17,4,3,5,2};
	int n=arr.length;

	int newarr[]=leaders(arr,n);

	for(int i=0;i<newarr.length;i++){
		System.out.print(newarr[i] +" ");
	}
   }
}
/*//{ Driver Code Starts
import java.io.*;
import java.util.*;
import java.util.stream.*;

class Array {
    
	public static void main (String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter ot = new PrintWriter(System.out);
		int t = Integer.parseInt(br.readLine().trim()); //Inputting the testcases
		
		while(t-->0){
		    
		    //input size of array
		    int n = Integer.parseInt(br.readLine().trim());
		    int arr[] = new int[n];
		    String inputLine[] = br.readLine().trim().split(" ");
		    
		    //inserting elements in the array
		    for(int i=0; i<n; i++){
		        arr[i] = Integer.parseInt(inputLine[i]);
		    }
		    
		    Solution obj = new Solution();
		    
		    StringBuffer str = new StringBuffer();
		    ArrayList<Integer> res = new ArrayList<Integer>();
		  
		    //calling leaders() function
		    res = obj.leaders(arr, n);
		    

		    for(int i=0; i<res.size(); i++){
		        ot.print(res.get(i)+" ");
		    }
		    
		    ot.println();
		}
		ot.close();
		
	}
}

// } Driver Code Ends


class Solution{
    //Function to find the leaders in the array.
    static ArrayList<Integer> leaders(int arr[], int n){
       ArrayList<Integer>list=new ArrayList<>();
       
       list.add(arr[n-1]);
       int leader=arr[n-1];
       
       for(int i=n-2;i>=0;i--){
           if(arr[i]>=leader){
               list.add(arr[i]);
               leader=arr[i];
           }
       }
       Collections.reverse(list);
       
       return list;
    }
}

