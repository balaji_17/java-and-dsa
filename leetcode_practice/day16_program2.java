//binarysearch
/*class solution162{

	public static void main(String [] args){

		int arr[]=new int[]{2,3,4,1,6,7,8};
		int n=arr.length;
		int X=4;
		System.out.println(Binary_search(arr,X,n));
	}

	public static int Binary_search(int [] arr,int X,int n){

		int start=0;
		int end=n-1;

		while(start<=end){
		 	int mid=start+(end-start)/2;

			if(arr[mid]==X){
				return mid; 
			}else if(arr[mid]< X){

				start=mid+1;
			}else{
				end=mid-1;
			}
		}
		return -1;
	}
}*/

class Solution162 {

    public static void main(String[] args) {
        int arr[] = new int[]{2, 3, 4, 1, 6, 7, 8};
        int n = arr.length;
        int X = 4;
        System.out.println(Binary_search(arr, X, n));
    }

    public static int Binary_search(int[] arr, int X, int n) {
        int start = 0;
        int end = n - 1;

        while (start <= end) {
            int mid = start + (end - start) / 2;

            if (arr[mid] == X) {
                return mid;
            } else if (arr[mid] < X) {
                start = mid + 1;
            } else {
                end = mid - 1;
            }
        }
        return -1;
    }
}

