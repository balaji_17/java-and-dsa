/*
 Code 1: Majority Element
Company: Flipkart, Accolite, Amazon, Microsoft, D-E-Shaw, Google, Nagarro, Atlassian
Platform : Leetcode - 169, GFG
Fraz’s & striver’s SDE sheet.
Description

Given an array nums of size n, return the majority element.
The majority element is the element that appears more than [n / 2⌋ times. You
may assume that the majority element always exists in the array.
Example 1:
Input: nums = [3,2,3]
Output: 3
Example 2:
Input: nums = [2,2,1,1,1,2,2]
Output: 2
Constraints:
n == nums.length
1 <= n <= 5 * 104
-109 <= nums[i] <= 109
 */

class majority{

	public static void main(String[] args){
		int arr[]=new int[]{1,2,2,2,1,1,1,1,2,2,2,1,1,2,2,3,3,3,4,5,6,7,8,8};

		System.out.println(majoirity_ele(arr));
	}
	static int majoirity_ele(int [] arr){
		int newcount=0;
		int majorityelement=-1;
		for(int i=0;i<arr.length;i++){
			int num=arr[i];
			int count=0;
			for(int j=i+1;j<arr.length;j++){
				if(arr[j]==num){
					count++;
				}
			}
			if(count>=newcount){
				newcount=count;
				majorityelement=num;
			}	
		}
		return majorityelement;
	}
}


