/*Code2 : Equal Left and Right Subarray Sum
Company : Amazon, Adobe
Platform : GFG
Description :
Given an array A of n positive numbers. The task is to find the first index in the
array such that the sum of elements before it equals the sum of elements after it.
Note: Array is 1-based indexed.
Example 1:
Input:
n = 5
A[] = {1,3,5,2,2}
Output: 3
Explanation: For second test case at position 3 elements before it (1+3) =
elements after it (2+2).

Example 2:
Input:

n = 1
A[] = {1}
Output: 1
Explanation: Since it's the only element hence it is the only point.
Expected Time Complexity: O(N)
Expected Space Complexity: O(1)
Constraints:
1 <= n <= 106
1 <= A[i] <= 108*/

class Solution2{
	int equalSum(int [] A, int N) {
	    if(N==1){
	        return 1;
	    }
	    
	    int totalsum=0;
	    for(int num:A){
	        totalsum+=num;
	    }
	    
	    int leftsum=0;
	    for(int i=0;i<N;i++){
	        if(leftsum==totalsum-leftsum-A[i]){
	            return i+1;
	        }
	        leftsum=leftsum+A[i];
	    }
	    return -1;
	}
}
