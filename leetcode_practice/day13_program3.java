class solution{ 
 int minDist(int a[], int n, int x, int y) {	
	int minDistance = Integer.MAX_VALUE;
        	int lastIndexX = -1;
        	int lastIndexY = -1;

        for (int i = 0; i < n; i++) {
            if (a[i] == x) {
                lastIndexX = i;
                if (lastIndexY != -1) {
                    minDistance = Math.min(minDistance, lastIndexX - lastIndexY);
                }
            } else if (a[i] == y) {
                lastIndexY = i;
                if (lastIndexX != -1) {
                    minDistance = Math.min(minDistance, lastIndexY - lastIndexX);
                }
            }
        }

        if (minDistance == Integer.MAX_VALUE) {
            return -1; // x or y not found
        }

        return minDistance;
	}
}
