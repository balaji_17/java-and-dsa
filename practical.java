import java.util.*;
class n_queen_problem 
{
	
	void printSolution(int board[][])
	{
		int N=board[0].length;
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				if (board[i][j] == 1)
					System.out.print("Q ");
				else
					System.out.print(". ");
			}
			System.out.println();
		}
	}
	boolean isSafe(int board[][], int row, int col)
	{
		int i, j;
		int N=board[0].length;
		// Check this row on left side
		for (i = 0; i < col; i++)
			if (board[row][i] == 1)
				return false;

		// Check upper diagonal on left side
		for (i = row, j = col; i >= 0 && j >= 0; i--, j--)
			if (board[i][j] == 1)
				return false;

		// Check lower diagonal on left side
		for (i = row, j = col; j >= 0 && i < N; i++, j--)
			if (board[i][j] == 1)
				return false;

		return true;
	}
	boolean solveNQUtil(int board[][], int col)
	{
		int N=board[0].length;
		if (col >= N)
			return true;
		
		for (int i = 0; i < N; i++) 
		{
			if (isSafe(board, i, col)) 
			{
				board[i][col] = 1;
				if (solveNQUtil(board, col + 1) == true)
					return true;
				board[i][col] = 0;
			}
		}
		return false;
	}

	boolean solveNQ()
	{
		System.out.println("How many Queen You want insert into the Board :-");
		Scanner sc=new Scanner(System.in);
		int n=sc.nextInt();
		int board[][]=new int[n][n];
		

		if (solveNQUtil(board, 0) == false) {
			System.out.print("Solution does not exist");
			return false;
		}

		printSolution(board);
		return true;
	}

	// Driver program to test above function
	public static void main(String args[])
	{
		n_queen_problem  Queen = new n_queen_problem ();
		Queen.solveNQ();
	}
}


