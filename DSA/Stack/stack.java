//LIFO
import java.util.*;
class Stackdemo{
	int maxSize;
	int stackArr[];
	int top=-1;

	Stackdemo(int size){
		this.stackArr=new int[size];
		this.maxSize=size;
	}

	void push(int data){
		if(top==maxSize-1){

			System.out.println("stack overflow");
			return;
		}else{
			top++;
			stackArr[top]=data;

		}
	}

	boolean empty(){

		if(top==-1)
			return true;
		else
			return false;

	}

	int Pop(){

		if(empty()){
			System.out.println("stack is empty");
			return -1;
		}else{
			int val = stackArr[top];
			top--;
			return val;
		}
	}
	int Peek(){

                if(empty()){
                        System.out.println("stack is empty");
                        return -1;
                }else{
                        int val = stackArr[top];
                        top--;
                        return val;
                }
     

	}
	int size(){

		return top;
	}

	void printStack(){

		if(empty()){
			System.out.println("Nothing to print");
			return;
		}else{
			System.out.println("[ ");
			for(int val=0;val<=top;val++){
				System.out.print(stackArr[val]+ " ");
			}
			System.out.println("]");
		}
	}
}


class client{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("enter stack size");

		int size=sc.nextInt();
		Stackdemo s=new Stackdemo(size);
		

		char ch;

		do{
			System.out.println("1.push");
			System.out.println("2.pop");
			System.out.println("3.peek");
			System.out.println("4.empty");
			System.out.println("5.size");
		 	System.out.println("6.printStack");	

			System.out.println("enter your choice");
			int choice=sc.nextInt();

			switch(choice){
				case 1:{
					       System.out.println("enter elemets for stack");
					       int data=sc.nextInt();
					       s.push(data);
				}
				break;

				case 2:{
					       int flag=s.Pop();
					       if(flag!=-1){
						       System.out.println(flag+"poped");
					       }
				}
				break;
				case 3:{
                                               int flag=s.Peek();
                                               if(flag!=-1){
                                                       System.out.println(flag);
                                               }
                                }

				break;

				case 4:{
					       boolean val=s.empty();
					       if(val)
						       System.out.println("empty");
					       else
						       System.out.println("not empty");
				}
				break;
				case 5:{
					       int sz=s.size();
					       System.out.println("stack size"+(sz+1));
				}
				break;
				default:
						System.out.println("invalid choise");
						break;
			}
			System.out.println("do you to contiune");
			ch=sc.next().charAt(0);
		}while(ch=='y'  || ch=='Y');

}
}

