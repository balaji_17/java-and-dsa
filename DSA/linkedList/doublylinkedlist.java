import java.util.*;

class Node {
    Node prev = null;
    int data;
    Node next = null;

    Node(int data) {
        this.data = data;
    }
}

class doublylinedlist {
    Node head = null;

    void addFirst(int data) {
        Node newNode = new Node(data);

        if (head == null) {
            head = newNode;
        } else {
            newNode.next = head;
            head.prev = newNode;
            head = newNode;
        }
    }

    void addLast(int data) {
        Node newNode = new Node(data);

        if (head == null) {
            head = newNode;
        } else {
            Node temp = head;
            while (temp.next != null) {
                temp = temp.next;
            }
            temp.next = newNode;
            newNode.prev = temp;
        }
    }

    int countNode() {
        int count = 0;
        if (head == null) {
            return count;
        } else {
            Node temp = head;
            while (temp != null) {
                count++;
                temp = temp.next;
            }

            return count;
        }
    }

    void addAtpos(int pos, int data) {
        if (pos <= 0 || pos >= countNode() + 2) {

            System.out.println("invalid pos");
        }
        if (pos == 1) {
            addFirst(data);
        } else if (pos == countNode() + 1) {
            addLast(data);
        } else {
            Node newNode = new Node(data);
            Node temp = head;
            while (pos - 2 != 0) {
                temp = temp.next;
                pos--;
            }
            newNode.next = temp;
            newNode.next = temp.next;
            temp.next = newNode;
            newNode.next.prev = newNode;
        }
    }

    void delFirst() {
        if (head == null) {
            System.out.println("ll is emplty");
        } else if (countNode() == 1) {
            head = null;
        } else {
            head = head.next;
            head.prev = null;
        }

    }

    void delLast() {
        if (head == null) {
            System.out.println("ll is emplty");
        } else if (countNode() == 1) {
            head = null;
        } else {
            Node temp=head;
            while(temp.next!=null){
                temp=temp.next;
            }
            temp.prev.next=null;
            temp.prev=null;
        }

    }

    void delAtpos(int pos) {
         if (pos <= 0 || pos >= countNode() + 1) {

            System.out.println("invalid pos");
        }
        if (pos == 1) {
            delFirst();
        } else if (pos == countNode()) {
            delLast();
        } else {
            Node temp=head;
            while(pos-2!=0){
                temp=temp.next;
                pos--;
            }
            temp.next=temp.next.next;
            temp.next.prev=temp;
            }

    }

    void printDLL() {
        if (head == null) {
            System.out.println("linkedlist is empty");
        } else {
            Node temp = head;
            while (temp.next != null) {
                System.out.println(temp.data + "->");
                temp = temp.next;
            }
            System.out.println(temp.data);
        }

    }
}

class client {
    public static void main(String[] args) {
        doublylinedlist ll = new doublylinedlist();

        char ch;
        do {
            System.out.println("singly linedlist");
            System.out.println("1.addFirst");
            System.out.println("2.addlast");
            System.out.println("3.addAtpos");
            System.out.println("4.delFirst");
            System.out.println("5.dellast");
            System.out.println("6.delAtpos");
            System.out.println("7.countNode");
            System.out.println("8.printfLL");

            System.out.println("enter your choice");
            Scanner sc = new Scanner(System.in);
            int choice = sc.nextInt();

            switch (choice) {
                case 1: {
                    System.out.println("enter data");
                    int data = sc.nextInt();
                    ll.addFirst(data);
                }
                    break;
                case 2: {
                    System.out.println("enter data");
                    int data = sc.nextInt();
                    ll.addLast(data);
                }
                    break;

                case 3: {
                    System.out.println("enter data");
                    int data = sc.nextInt();
                    System.out.println("enter pos");
                    int pos = sc.nextInt();
                    ll.addAtpos(pos, data);
                }
                    break;

                case 4:
                    ll.delFirst();
                    break;
                case 5:
                    ll.delLast();
                    break;
                case 6: {
                    System.out.println("enter pos");
                    int pos = sc.nextInt();
                    ll.delAtpos(pos);
                }
                    break;
                case 7: {

                    int count = ll.countNode();
                    System.out.println(count);
                }
                    break;
                case 8:
                    ll.printDLL();
                    break;
                default:
                    System.out.println("wrong choice");
                    break;
            }

            System.out.println("contine");
            ch = sc.next().charAt(0);

        } while (ch == 'Y' || ch == 'y');
    }
}

