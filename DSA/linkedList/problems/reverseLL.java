import java.util.*;
class Node{
	int data;
	Node next=null;

	Node(int data){
		this.data=data;
	}
}
class LinkedList{
	Node head=null;

	//addNode
	void addNode(int data){
		Node newNode=new Node(data);
		if(head==null){
			head=newNode;
		}else{
			Node temp=head;

			while(temp.next!=null){
				temp=temp.next;
			}
			temp.next=newNode;
		
		}

	}
	//print
	void printLL(){
		if(head==null){
			System.out.println("empty linkedlist");
			return;
		}else{

			Node temp=head;
			while(temp.next!=null){

				System.out.print(temp.data+"->");
				temp=temp.next;
			}
			System.out.println(temp.data);
		}
	
	}
	void reverseITR(){

		if(head==null){
			return;
		}
		if(head.next==null){
			return;
		}
		Node prev=null;
		Node curr=head;
		Node forword=null;

		while(curr!=null){
			forword=curr.next;
			curr.next=prev;
			prev=curr;
			curr=forword;
		}
		head=prev;

	}
	void reverseREC(Node prev,Node curr){
		if(curr==null){
			head=prev;
			return;
		}else{
			Node forward=curr.next;
			curr.next=prev;
			prev=curr;
			curr=forward;
		}
		reverseREC(prev,curr);


	}
}

class client{

	public static void main(String[] args){

	LinkedList ll = new LinkedList();
	Scanner sc=new Scanner(System.in);
	char ch;

	do{

		System.out.println("1.addNode");
		System.out.println("2.printLL");
		System.out.println("3.reverseITR");
                System.out.println("4.preverseREC");

		System.out.println("enter your choice");
		int choice=sc.nextInt();

		switch(choice){
		case 1:
			{
				System.out.println("enter data");
				int data=sc.nextInt();
				ll.addNode(data);
			}
			break;
		case 2:
			
				ll.printLL();
				break;

		case 3:
				ll.reverseITR();
				break;

		case 4:
				Node prev=null;
				ll.reverseREC(prev,ll.head);
				break;

		default:
				System.out.println("wrong choice");
				break;
			
		}
		System.out.println("do you want to continue?");
		ch=sc.next().charAt(0);
	}while(ch=='y' || ch=='Y');
	
	}
}

