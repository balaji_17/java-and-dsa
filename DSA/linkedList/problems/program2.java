//singly Linedlist
import java.util.*;

class Node{
	int data;
	Node next=null;

	Node(int data){
		this.data=data;
	}

}

class linkedLL{
	Node head=null;

	void addFirst(int data){
		Node newNode= new Node(data);

		if(head==null){
			head=newNode;
		}else{
			newNode.next=head;
			head=newNode;
		}


	}

	void addLast(int data){
		Node newNode=new Node(data);

		if(head==null){
			System.out.println("empty LL");
		}else{
		Node temp=head;
		while(temp.next!=null){
			temp=temp.next;
		}
		temp.next=newNode;	
		}


	}

	int countNode(){
		int count=0;
		if(head==null){
			return count;
		}else{
			Node temp=head;
			while(temp!=null){
				count++;
				temp=temp.next;
			}
		}
		return count;
	}

	void addAtpos(int data,int pos){
		if(pos<=0 || pos>=countNode()+2){
			System.out.println("invalid pos");
			return;
		}
		if(pos==1){
			addFirst(data);
		}
		else if(pos==countNode()+1){
			addLast(data);
		}else{
			Node newNode=new Node(data);

			Node temp=head;

			while(pos-2!=0){
				temp=temp.next;
				pos--;
			}
			newNode.next=temp.next;
			temp.next=newNode;

		}
	
	}

	void delFirst(){
		if(head==null){
			System.out.println("LL is empty");
		}
		if(countNode()==1){
			head=null;
		}else{
			head=head.next;
		}
	
		}
	void delLast(){
	if(head==null){
		System.out.println("LL is empty");
        }
	if(countNode()==1){
		head=null;
	}else{

			Node temp=head;

			while(temp.next.next!=null){

				temp=temp.next;
			}
			temp.next=null;
	       }

	}

	void printLL(){
		if(head==null){
			System.out.println("emprty linked");
		}else{
			Node temp=head;
			while(temp.next!=null){

				System.out.print(temp.data+"->");
				temp=temp.next;
			}
			System.out.print(temp.data);
		}
	}


}

class client{
	public static void main(String [] args){
		linkedLL LL = new linkedLL();
		char ch;
		do{
			System.out.println("1.addFirst");
			System.out.println("2.addLast");
			System.out.println("3.addAtpos");
                        System.out.println("4.delfirst");
			System.out.println("5.dellast");
                        System.out.println("6.delAtpos");
			System.out.println("7.countNode");
                        System.out.println("8.printLL");

			System.out.println("Enter your choice");
			Scanner sc =new Scanner(System.in);
			
			int choice=sc.nextInt();

			switch(choice){
				case 1:
					{
						System.out.println("enter data");
						int data=sc.nextInt();
						LL.addFirst(data);

					}
					break;
				case 2:
					{
					 System.out.println("enter data");
                                                int data=sc.nextInt();
                                                LL.addLast(data);
					
					}
					break;

				case 3:{	
					 System.out.println("enter data");
					 int data=sc.nextInt();
					 System.out.println("enter position");
					 int pos=sc.nextInt();
					 LL.addAtpos(data,pos);

					}
					break;
				case 4:
					LL.delFirst();
					break;
				case 7:{

					int count=LL.countNode();
					System.out.println("count is "+ count);
					}
				       break;

				case 8:
					LL.printLL();
					break;
				default:
					System.out.println("invalid");


			}
					
			System.out.println("do you want to continue");
			ch=sc.next().charAt(0);
		}while(ch=='y' || ch=='Y');
		

	}

}
