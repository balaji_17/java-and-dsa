class Node {
    int data;
    Node next=null;

    Node(int data){
        this.data=data;
    }
}

class Linkedlist{
    Node head=null;

    void addFirst(int data){
    Node newNode=new Node(data);

    if(head==null){
        head=newNode;
    }else{
        newNode.next=head;
        head=newNode;
    }
    }

    int countNode(){
        int count=0;
        Node temp=head;
        while(temp!=null){
            count++;
            temp=temp.next;
        }
        return count;
    }
}
class Main{
    public static void main(String[] args){
        Linkedlist obj=new Linkedlist();

        obj.addFirst(10);
        obj.addFirst(20);
        obj.addFirst(30);
        obj.addFirst(40);
        int count=(obj.countNode());
        System.out.println(count);
        
    }
}
