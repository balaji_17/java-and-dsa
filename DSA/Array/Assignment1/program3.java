/*Q3. Range Sum Query

Problem Description
- You are given an integer array A of length N.
- You are also given a 2D integer array B with dimensions M x 2, where

each row

denotes a [L, R] query.
- For each query, you have to find the sum of all elements from L to R

indices

in A (0 - indexed).
- More formally, find A[L] + A[L + 1] + A[L + 2] +... + A[R - 1] + A[R] for each
query.

Problem Constraints
1 <= N, M <= 103
1 <= A[i] <= 105
0 <= L <= R < N

Example Input
Input 1:
A = [1, 2, 3, 4, 5]
B = [[0, 3], [1, 2]]
Input 2:
A = [2, 2, 2]
B = [[0, 0], [1, 2]]

Example Output
Output 1:
[10, 5]

Output 2:
[2, 4]

Example Explanation
Explanation 1:
The sum of all elements of A[0 ... 3] = 1 + 2 + 3 + 4 = 10.
The sum of all elements of A[1 ... 2] = 2 + 3 = 5.
Explanation 2:
The sum of all elements of A[0 ... 0] = 2 = 2.
The sum of all elements of A[1 ... 2] = 2 + 2 = 4.
*/
import java.util.*;
class Demo3{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);

                System.out.println("size");
                int size=sc.nextInt();

                int arr[]=new int[size];
                System.out.println("elemets");
                for(int i=0;i<size;i++){
                        arr[i]=sc.nextInt();
                }
                System.out.println("enetr query ");
                int Q=sc.nextInt();
                int[][] query=new int[Q][2];
                System.out.println("enter the S and E");
		for(int i=0;i<Q;i++){
			query[i][0]=sc.nextInt();
			query[i][1]=sc.nextInt();
		}
		int [] arr1=query_sum(size,arr,Q,query);
		for(int i=0;i<arr1.length;i++){
			System.out.print(arr[i]+" ");
		}
	}

	public static int[] query_sum(int size,int[]arr,int Q,int[][] query){

		int arr1[] =new int [Q];
		int sum=0;

        	for (int i = 0; i < size; i++) {
            		sum = sum + arr[i];
            		arr[i] = sum;
        	
		  }

		for(int i=0;i<Q;i++){
			int S=query[i][0];
			int E=query[i][1];

		
			for(int j=S;j<=E;j++){

				sum1=sum1+arr
			}
			arr[i]=sum1;
		}
		return arr1;
	
	}
}
	/*
import java.util.Scanner;

class Demo3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("size");
        int size = sc.nextInt();

        int arr[] = new int[size];
        System.out.println("elements");
        for (int i = 0; i < size; i++) {
            arr[i] = sc.nextInt();
        }

        int[] arr1 = query_sum(size, arr);

        for (int i = 0; i < arr1.length; i++) {
            System.out.print(arr1[i] + " ");
        }
    }

    public static int[] query_sum(int size, int[] arr) {
        Scanner sc = new Scanner(System.in);
        System.out.println("query");
        int Q = sc.nextInt();
        int arr1[] = new int[Q + 1]; // Change the size to Q + 1
        int sum = 0;
        for (int i = 0; i < size; i++) {
            sum = sum + arr[i];
            arr[i] = sum;
        }

        int num = 0;
        for (int i = 1; i <= Q; i++) {
            System.out.println("enter S and E");
            int S = sc.nextInt();
            int E = sc.nextInt();

            if (S == 0) {
                num = arr[E];
            } else {
                num = arr[E] - arr[S - 1];
            }
            arr1[i] = num;
        }
        return arr1;
    }
}*/



        
