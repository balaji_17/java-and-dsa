/*Q1. Max Min of an Array

Problem Description
- Given an array A of size N.
- You need to find the sum of the Maximum and Minimum

elements in the given array.

Problem Constraints
1 <= N <= 105
-109 <= A[i] <= 109

Example Input
Input 1:
A = [-2, 1, -4, 5, 3]
Input 2:
A = [1, 3, 4, 1]

Example Output
Output 1:
1
Output 2:
5
Example Explanation
Explanation 1:
Maximum Element is 5 and Minimum element is -4. (5 + (-4)) = 1.
Explanation 2:
Maximum Element is 4 and Minimum element is 1. (4 + 1) = 5.*/
import java.util.*;
class Demo1{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.println("size");
		int size=sc.nextInt();

		int arr[]=new int[size];
		System.out.println("elemets");
		for(int i=0;i<size;i++){
			arr[i]=sc.nextInt();
		}
		int n=sum_maxmin(size,arr);
		System.out.println("sum is:"+n);
	}
	public static int sum_maxmin(int size,int[] arr){
		int sum=0;
		for(int i=0;i<size;i++){
			sum=sum+arr[i];
			arr[i]=sum;
		}
		int num=arr[0]+arr[size-1];
		return num;
	}
}

