/* given of array size n return count pair(i,j) with arr[i]+arr[j] and i!=j
 */

class arraydemo3{
	public static void main(String[] args){
		int size=12;
		int K=11;
		int [] arr=new int[] {10,12,10,15,-1,7,6,5,4,2,1,1,1};
		int count=0;
		for(int i=0;i<size;i++){
			for(int j=0;j<size;j++){
				if(arr[i]+arr[j]==K && i!=j){
					count++;
				}
			}
		}
		System.out.println(count);
	}
}
