// reverse array
class arraydemo4{
	public static void main(String[] args){
		int size=5;
		int [] arr=new int[]{1,2,3,4,5};
		int start=0;
		int end=size-1;

		while(start<end){
			int temp=arr[start];
			arr[start]=arr[end];
			arr[end]=temp;

			start++;
			end--;
		}
		for(int i=0;i<size;i++){
			System.out.print(arr[i]+" ");
		}
	}
}
