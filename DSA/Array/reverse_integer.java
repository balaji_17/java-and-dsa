/*1. Reverse Integer (Leetcode:- 7)

Given a signed 32-bit integer x, return x with its digits reversed. If reversing
x causes the value to go outside the signed 32-bit integer range [-231, 231
- 1], then return 0.
Assume the environment does not allow you to store 64-bit integers (signed
or unsigned).
Example 1:
Input: x = 123
Output: 321
Example 2:
Input: x = -123
Output: -321
Example 3:
Input: x = 120
Output: 21

Constraints:
-231 <= x <= 231 - 1
*/

/*import java.util.*;
class reveresedemo{
	public static void main(String[] args){
		Scanner sc =new Scanner(System.in);
		System.out.println("enter size");
		int size=sc.nextInt();
		int[] arr=new int[size];
		int n=arr.length;
		System.out.println("enter elemets");
		for(int i=0;i<n;i++){
			arr[i]=sc.nextInt();
		}
		System.out.println("array");
                for(int oldarr:arr){
                        System.out.print(oldarr+" ");
                }
		System.out.println();
		int i=0;
		int j=n-1;

		while(i<=j){
			int temp=arr[i];
			arr[i]=arr[j];
			arr[j]=temp;

			i++;
			j--;
		}
		System.out.println("reverse array");
		for(int newarr:arr){
			System.out.print(newarr+" ");
		}
	}
}*/
import java.util.*;
class reversedemo1{
	public static void main(String [] args){
		Scanner sc =new Scanner(System.in);
                System.out.println("enter integer");
                int num=sc.nextInt();

		int n=reverse_int( num);
		System.out.println(n);
	}
	public static int reverse_int(int num){
		
		int rev=0;
		while(num!=0){
		int rem=num%10;
		rev=(rev*10)+rem;
		num=num/10;
		}
		return rev;
	}
}
		
