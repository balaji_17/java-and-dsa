/*. Search Insert Position (LeetCode-35)

Given a sorted array of distinct integers and a target value, return the index
if the target is found. If not, return the index where it would be if it were
inserted in order.
You must write an algorithm with O(log n) runtime complexity.
Example 1:
Input: nums = [1,3,5,6], target = 5
Output: 2
Example 2:
Input: nums = [1,3,5,6], target = 2
Output: 1
Example 3:
Input: nums = [1,3,5,6], target = 7
Output: 4

Constraints:
1 <= nums.length <= 104
-104 <= nums[i] <= 104
nums contains distinct values sorted in ascending order.
-104 <= target <= 104
*/

import java.util.*;
class searchdemo{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("size");

		int size=sc.nextInt();

		int[]arr=new int[size];
		System.out.println("target");

		int target=sc.nextInt();

		for(int i=0;i<size;i++){
			arr[i]=sc.nextInt();
		}
		System.out.println(search_ele(size,arr,target));
	}
	public static int search_ele(int size,int[] arr,int target){
		int left=0;
		int right=size-1;
		while(left<=right){
			int mid=left+(right-left)/2;
			if(arr[mid]==target){
				return mid;
			}
			else if(arr[mid]<target){
				left=mid+1;
			}
			else{
				right=mid-1;
			}
		}
		return left;
	}
}


