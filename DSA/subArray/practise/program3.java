// given an array of size N
// print the sum of every single elemets
class demo3{
        public static void main(String[] args){
                int[] arr=new int[]{2,4,1,3};
		int n=arr.length;
		int[] presum=new int[n];
                presum[0]=arr[0];
		for(int i=1;i<n;i++){
			presum[i]=presum[i-1]+arr[i];
		}
		for(int i=0;i<n;i++){
			for(int j=i;j<n;j++){
				if(i==0){
					System.out.println(presum[j]);
				}else{
					System.out.println(presum[j]-presum[i-1]);
				}
			}
		}

        }
}
