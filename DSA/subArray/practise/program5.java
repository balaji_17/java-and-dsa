//find the largest sum in continous subarray
class demo5{
	public static void main(String[] args){
		int [] arr=new int[]{-2,1,-3,4,-1,2,1,-5,4};
		int n=arr.length;

		int max=Integer.MIN_VALUE;
		for(int i=1;i<n;i++){
			arr[i]=arr[i-1]+arr[i];
		}

		for(int i=0;i<n;i++){
			int sum=0;
			for(int j=i;j<n;j++){
				if(i==0){
					sum=arr[j];
				}else{
					sum=arr[j]-arr[i-1];
				}
				if(sum>max){
					max=sum;
				}
			}
		}
		System.out.println(max);
	}
}
