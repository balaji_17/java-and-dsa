/*Given an array containing n integers. The problem is to find the sum of the
elements of the contiguous subarray having the smallest(minimum) sum.
Examples:
Input : arr[] = {3, -4, 2, -3, -1, 7, -5}
Output : -6
Subarray is {-4, 2, -3, -1} = -6
Input : arr = {2, 6, 8, 1, 4}
Output : 1
*/
class min_sum{
	public static void main(String[] args){
		int [] arr=new int[]{3,-4,2,-3,-1,7,5};
		int n=arr.length;
		int min=Integer.MIN_VALUE;
		for(int i=0;i<n;i++){
			int sum=0;
			for(int j=1;j<n;j++){
				sum=sum+arr[j];
				if(min<sum){
					min=sum;
				}
			}
		}
		System.out.println(min);
	}
}
