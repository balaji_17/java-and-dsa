/*Problem 4:
Given an array of positive integers nums and a positive integer target, return the minimal length
of a
subarray
whose sum is greater than or equal to target. If there is no such subarray, return 0 instead.

Example 1:
Input: target = 7, nums = [2,3,1,2,4,3]
Output: 2
Explanation: The subarray [4,3] has the minimal length under the problem constraint.
Example 2:
Input: target = 4, nums = [1,4,4]
Output: 1
Example 3:
Input: target = 11, nums = [1,1,1,1,1,1,1,1]
Output: 0

Constraints:
1 <= target <= 109
1 <= nums.length <= 105
1 <= nums[i] <= 104
*/

class demo4{
	public static void main(String[] args){
		int [] arr=new int[]{2,3,1,2,4,3};
		int n=arr.length;
		int target=7;
		int num=0;
		int min=Integer.MAX_VALUE;
		for(int i=0;i<n;i++){
			int sum=0;
			if(arr[i]==target){
				System.out.println(i);
				break;
			}
			for(int j=i;j<n;j++){
				sum=sum+arr[j];
				if(sum==target){
					num=j-i+1;
					if(min<num){
						num=min;
					}
				}
			}
		}
		System.out.println(num);
	}
}
