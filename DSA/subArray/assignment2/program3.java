// Equal left and Equal right subArray
import java.util.*;

class demo3{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("size");
		int size=sc.nextInt();

		int arr[]=new int[size];

		System.out.println("elemts");

		for(int i=0;i<size;i++){
			arr[i]=sc.nextInt();
		}
		
		int num=equal_leftRight(size,arr);
		System.out.println("num:"+num);
	}
	public static int equal_leftRight(int size,int[] arr){
		int[] prefix_sum=new int[size];
		int[] suffix_sum=new int[size];

		prefix_sum[0]=arr[0];
		suffix_sum[size-1]=arr[size-1];
		for(int i=1;i<size;i++){
			prefix_sum[i]=prefix_sum[i-1]+arr[i];
		}
		for(int i=size-2;i>=0;i--){

			suffix_sum[i]=suffix_sum[i+1]+arr[i];
		
		}
		for(int i=0;i<size;i++){
			if(suffix_sum[i]==prefix_sum[i]){
				return arr[i];
			}
		}
		return -1;
	}
}

