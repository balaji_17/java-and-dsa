class selectionsort{
	public static void main(String[] args){
		int arr[]=new int[]{9,2,7,3,1,8,4,6};

		for(int i=0;i<arr.length-1;i++){
			int midIndex=i;

			for(int j=i+1;j<arr.length-1;j++){
				if(arr[j]<arr[midIndex]){
					midIndex=j;
				}
			}
			int temp=arr[i];
			arr[i]=arr[midIndex];
			arr[midIndex]=temp;
		}
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
		}
	}
}
