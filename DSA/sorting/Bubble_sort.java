//Bubble sort.
/*class bubblesort{
	public static void main(String[] args){
		int[] arr=new int[]{7,3,9,4,2,5,6};

		for(int i=0;i<arr.length;i++){

			for(int j=0;j<arr.length-i-1;j++){
				boolean swapped=false;

				if(arr[j]>arr[j+1]){

					int temp=arr[j];
					arr[j]=arr[j+1];
					arr[j+1]=temp;
					swapped=true;

				}
		
			if(swapped==false)
				break;
		}
		}
		for(int i=0;i<arr.length;i++){

			System.out.print(arr[i]+" ");
		}

	}
}	*/
class BubbleSort{
	public static void main(String[] args){
		BubbleSort obj=new BubbleSort();
		int [] arr=new int[]{7,3,9,4,2,5,6};
		obj.bubblesort(arr);

		 for(int i=0;i<arr.length;i++){

                        System.out.print(arr[i]+" ");
                }

	}

int [] bubblesort(int [] arr){
	for(int i=0;i<arr.length;i++){
		boolean swapped=false;
		for(int j=0;j<arr.length-i-1;j++){


			if(arr[j]>arr[j+1]){
				int temp=arr[j];
				arr[j]=arr[j+1];
				arr[j+1]=temp;
				swapped=true;
			}
		}
			if(swapped==false)
				break;
	}
	return arr;
}
}
