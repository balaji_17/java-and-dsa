class countingSort{

	int getMax(int[] arr, int n) {
  	int max = arr[0];
  	for(int i = 1; i<n; i++) {
      		if(arr[i] > max)
        	 max = arr[i];
  	}
  	return max; //maximum element from the array
}
	
	void counting(int arr[],int n){
		int output[]=new int[n+1];
		int max=getMax(arr,n);

		int count[]=new int[max+1];

		for(int i=0;i<=max;++i){
			count[i]=0;
		}

		for(int i=0;i<n;i++){

			count[arr[i]]++;
		}

		for(int i=1;i<=max;i++){
			count[i]+=count[i-1];

		}

		for(int i=n-1;i>=0;i--){
			output[count[arr[i]]-1]=arr[i];
			count[arr[i]]--;
		}

		for(int i=0;i<n;i++){
			arr[i]=output[i];
		}
	}

	void printArr(int arr[],int n){

		int i=0;
		for(i=0;i<n;i++){
			System.out.print(arr[i]+ " ");
		}
	}
	public static void main(String[] args){
		int arr[]=new int[]{ 11, 30, 24, 7, 31, 16, 39, 41};
		int n=arr.length;
		countingSort obj=new countingSort();
		
		System.out.println("Before sort");
		obj.printArr(arr,n);
		obj.counting(arr,n);
	
		System.out.println("after sort");
		obj.printArr(arr,n);

	}

}
