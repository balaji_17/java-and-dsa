class LinearSearch{

	public static void main(String[] args){
		int arr[]=new int[]{1,2,3,4,5,6,7};
		int N=arr.length;
		int data=5;

		int result=LinearSearch(arr,N,data);

		if(result!=-1){
			System.out.println("element fount "+ result);
		}else{
			System.out.println("not found");
		}

	}

	public static int LinearSearch(int [] arr,int N,int data){

		for(int i=0;i<N;i++){

			if(arr[i]==data){
				return i;
			}
		}
		return -1;
	}
}
