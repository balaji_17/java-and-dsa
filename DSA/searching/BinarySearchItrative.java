class BinarySearchITR{

        public static void main(String[] args){
                int arr[]=new int[]{1,2,3,4,5,6,7};
                int N=arr.length;
                int data=5;

                int result=BinarySearchITR(arr,N,data);

                if(result!=-1){
                        System.out.println("element fount "+ result);
                }else{
                        System.out.println("not found");
                }

        }
	public static int BinarySearchITR(int arr[],int N,int data){
		int low=0;
		int high=N-1;

		while(low<=high){
			int mid=low+(high-low)/2;

			if(arr[mid]==data){
				return mid;
			}else if(arr[mid]<data){
				low=mid+1;
			}else{
				high=mid-1;
			}

		}
		return -1;
	
	}
}
