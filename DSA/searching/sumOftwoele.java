import java.util.*;

class Main {
    public static void main(String[] args) {
    int arr[]=new int[]{1,2,3,4,5,6,7,8,9,10};
    int N=arr.length;
    int sum=15;
    
    searchsum(arr,N,sum);
    
  }
  
  public static void searchsum(int arr[],int N,int sum){
    int left=0;
    int right=N-1;
    int newsum;
    
    for(;left<right;){
      newsum=arr[left]+arr[right];
      
      if(newsum==sum){
          System.out.println("Indices: " + left + ", " + right);
                return;     
      }else if(newsum<sum){
        left=left+1;
      }else{
        right=right-1;
      }
    }
    System.out.println("No such pair found.");
  }
}
