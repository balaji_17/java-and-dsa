class BinarySearchREC {

    public static void main(String[] args) {
        int arr[] = new int[]{1, 2, 3, 4, 5, 6, 7};
        int N = arr.length;
        int data = 5;

        int result = BinarySearchREC(arr, 0, N - 1, data);

        if (result != -1) {
            System.out.println("Element found at index " + result);
        } else {
            System.out.println("Element not found");
        }

    }

    public static int BinarySearchREC(int arr[], int low, int high, int data) {
        if (low > high)
            return -1;

        int mid = low + (high - low) / 2;

        if (arr[mid] == data)
            return mid;
        else if (arr[mid] < data)
            return BinarySearchREC(arr, mid + 1, high, data);
        else
            return BinarySearchREC(arr, low, mid - 1, data);
    }
}

