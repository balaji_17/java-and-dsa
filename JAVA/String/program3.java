// toString method
class student{
	int rollNo;
	String name;
	String city;

	student(int rollNo,String name,String city){

		this.rollNo=rollNo;
		this.name=name;
		this.city=city;
	}

	public String toString(){

		return rollNo+" "+name+" "+city;
	}

	public static void main(String[] args){
		student str1= new student(1,"balaji","barshi");
		student str2= new student(2,"shashi","pune");

		System.out.println(str1);
		System.out.println(str2);		
		
	}
}
