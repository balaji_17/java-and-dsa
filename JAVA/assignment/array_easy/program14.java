/*Que 18 : Move all zero two the end of Array
Given an array arr[] of N positive integers. Push all the zeros of the given array to the
right end of the array while maintaining the order of non-zero elements.
Example 1:
Input:
N = 5
Arr[] = {3, 5, 0, 0, 4}
Output: 3 5 4 0 0
Explanation: The non-zero elements preserve their order while the 0 elements are
moved to the right.
Example 2:
Input:
N = 4
Arr[] = {0, 0, 0, 4}
Output: 4 0 0 0
Explanation: 4 is the only non-zero element and it gets moved to the left.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 ≤ N ≤ 105
0 ≤ arri ≤ 105
*/

import java.util.*;
class demo15{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.println("size1");
                int size=sc.nextInt();
                int[] arr=new int[size];
                System.out.println("elements1");

                for(int i=0;i<size;i++){
                        arr[i]=sc.nextInt();
                }
		move_zero(size,arr);
		for(int num:arr){
			System.out.print(num+" ");
		}
	}
	public static int[] move_zero(int size,int[] arr){
	       int nonzeroindex=0;
		for(int i=0;i<size;i++){
		       	if(arr[i]!=0){
			int temp=arr[i];
		        arr[i]=arr[nonzeroindex];
	       	        arr[nonzeroindex]=temp;
			nonzeroindex++;
		       }
	       }

	       return arr;
	}
}

