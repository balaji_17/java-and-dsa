/*Que 13 : Minimum distance between two number
You are given an array A, of N elements. Find minimum index based distance between
two elements of the array, x and y such that (x!=y).
Example 1:
Input:
N = 4
A[] = {1,2,3,2}
x = 1, y = 2
Output: 1
Explanation: x = 1 and y = 2. There are two distances between x and y, which are
1 and 3 out of which the least is 1.
Example 2:
Input:
N = 7
A[] = {86,39,90,67,84,66,62}
x = 42, y = 12
Output: -1
Explanation: x = 42 and y = 12. We return -1 as x and y don't exist in the array.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 <= N <= 10
5
0 <= A[i], x, y <= 10
5
*/
import java.util.*;
import java.lang.Math;
class demo13{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("size");
		int size=sc.nextInt();
		System.out.println("X and Y");
		
		int X=sc.nextInt();
		int Y=sc.nextInt();

		int[] arr=new int[size];
		System.out.println("elements");

		for(int i=0;i<size;i++){
			arr[i]=sc.nextInt();
		}
		System.out.println("minimum distance is:"+minimum_dis(size,arr,X,Y));
	}
	public static int minimum_dis(int size,int[] arr,int X,int Y){
		int min=Integer.MAX_VALUE;
		int sub=0;
		/*
		for(int i=0;i<size-1;i++){
			if(arr[i]==X );
			for(int j=i+1;j<size;j++){
				if(arr[j]==Y){
					sub=j-i;
				}
				if(sub>min){
				sub=min;
				}
			}
			
		}
		return sub*/
		int a=-1;
		int b=-1;
		for(int i=0;i<size;i++){
			if(arr[i]==X){
				a=i;
			}
			if(arr[i]==Y){
				b=i;
			}
			if(min>Math.abs(a-b) && a<b){
				min=b;
			}
		}
		if(a==-1 || b==-1){
			return -1;
		}
		return min;
	}
}
