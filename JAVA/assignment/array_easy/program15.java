/*Que 21 : Non Repeating Element
Find the first non-repeating element in a given array Arr of N integers.
Note: Array consists of only positive and negative integers and not zero.
Example 1:
Input : arr[] = {-1, 2, -1, 3, 2}
Output : 3
Explanation: -1 and 2 are repeating whereas 3 is the only number occuring once.
Hence, the output is 3.
Example 2:
Input : arr[] = {1, 1, 1}
Output : 0
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)
Constraints:
1 <= N <= 10
7
-10
16 <= Ai <= 10
16
{Ai !=0 }
*/
import java.util.*;
class demo15{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.println("size1");
                int size=sc.nextInt();
                int[] arr=new int[size];
                System.out.println("elements1");

                for(int i=0;i<size;i++){
                        arr[i]=sc.nextInt();
                }
                int[] newarray=non_reapting(size,arr);
                for(int i=0;i<size;i++){
			if(newarray[i]!=0)
                        System.out.print(newarray[i]+" ");
                }

        }
        public static int[] non_reapting(int size,int[] arr){
               int[] newarray=new int[size];
	       int itr=0;
                for(int i=0;i<size;i++){
			for(int j=i+1;j<size;j++){
				if(arr[i]==arr[j]){
					newarray[i]=arr[i];
					itr++;
					break;
				}
			}
		}
		System.out.println("itreation "+itr);
		return newarray;
	}
}

