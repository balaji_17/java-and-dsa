/*Que 1 : Missing number in array
Given an array of size N-1 such that it only contains distinct integers in the range of 1 to
N. Find the missing element.
Example 1:
Input:
N = 6
A[] = {1,2,4,5,6}
Output: 3
Example 2:
Input:
N = 11
A[] = {1,3,2,5,6,7,8,11,10,4}
Output: 9
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 ≤ N ≤ 10
6
1 ≤ A[i] ≤ 10*/
import java.io.*;
import java.util.*;

class Demo1 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter size");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];
        System.out.println("Enter elements");
        for (int i = 0; i < arr.length; i++) {
            arr[i] = Integer.parseInt(br.readLine());
        }
        System.out.println("Missing element: " + missing_ele(size, arr));
    }

    public static int missing_ele(int size, int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != i + 1) {
                return i + 1;
            }
        }
        
        return size + 1;
    }
}


