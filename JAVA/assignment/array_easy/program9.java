/*Que 9 : Find transition Point
Given a sorted array containing only 0s and 1s, find the transition point.
Example 1:
Input:
N = 5
arr[] = {0,0,0,1,1}
Output: 3
Explanation: index 3 is the transition point where 1 begins.
Example 2:
Input:
N = 4
arr[] = {0,0,0,0}
Output: -1
Explanation: Since, there is no "1", the answer is -1.
Expected Time Complexity: O(LogN)
Expected Auxiliary Space: O(1)
Constraints:
1 ≤ N ≤ 500000
0 ≤ arr[i] ≤ 1
*/

import java.util.*;
class demo9{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("size");
		int size=sc.nextInt();
		int [] arr=new int[size];
		System.out.println("elemets");
		for(int i=0;i<size;i++){
			arr[i]=sc.nextInt();
		}
		System.out.println("transition point:"+transition_point(size,arr));
	}
	public static int transition_point(int size,int[] arr){
		for(int i=0;i<1;i++){
			for(int j=0;j<size;j++){
				if(arr[i]!=arr[j]){
					return j;
				}
			}
		}
		return 0;
	}
}
