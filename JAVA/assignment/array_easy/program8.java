/*Que 8 : Rotate Array
Given an unsorted array arr[] of size N. Rotate the array to the left (counter-clockwise
direction) by D steps, where D is a positive integer.
Example 1:
Input:
N = 5, D = 2
arr[] = {1,2,3,4,5}
Output: 3 4 5 1 2
Explanation: 1 2 3 4 5 when rotated
by 2 elements, it becomes 3 4 5 1 2.
Example 2:
Input:
N = 10, D = 3
arr[] = {2,4,6,8,10,12,14,16,18,20}
Output: 8 10 12 14 16 18 20 2 4 6
Explanation: 2 4 6 8 10 12 14 16 18 20
when rotated by 3 elements, it becomes
8 10 12 14 16 18 20 2 4 6.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 <= N <= 10
6
1 <= D <= 10
6
0 <= arr[i] <= 10
5
*/

import java.util.*;
class demo8{
        public static void main(String[] args){
                Scanner sc=new Scanner(System.in);
                System.out.println("size");
                int size=sc.nextInt();
                System.out.println("D");
                int D=sc.nextInt();
                int[] arr=new int[size];
                System.out.println("elemst");
                for(int i=0;i<size;i++){
                        arr[i]=sc.nextInt();
                }
                int [] temp=rotating_array(size,arr,D);
		for(int num:temp){
			System.out.print(num+" ");
		}

        }
	public static int[] rotating_array(int size,int[] arr,int D){
		int[] temp=new int[size];
		for(int i=0;i<size;i++){
			int newindex=(i+D)%size;
			temp[newindex]=arr[i];
		}
		return temp;
	}
}


