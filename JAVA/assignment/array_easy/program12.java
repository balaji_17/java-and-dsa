/*Que 14 : Intersection of two Array
Given two arrays a[] and b[] respectively of size n and m, the task is to print the count of
elements in the intersection (or common elements) of the two arrays.
For this question, the intersection of two arrays can be defined as the set containing
distinct common elements between the two arrays.
Example 1:
Input:
n = 5, m = 3
a[] = {89, 24, 75, 11, 23}
b[] = {89, 2, 4}
Output: 1
Explanation:
89 is the only element in the intersection of two arrays.
Example 2:
Input:
n = 6, m = 5
a[] = {1, 2, 3, 4, 5, 6}
b[] = {3, 4, 5, 6, 7}
Output: 4
Explanation:
3 4 5 and 6 are the elements in the intersection of two arrays.
Expected Time Complexity: O(n + m).
Expected Auxiliary Space: O(min(n,m)).
Constraints:
1 ≤ n, m ≤ 10
5
1 ≤ a[i], b[i] ≤ 10
5
*/
import java.util.*;
class demo14{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.println("size1");
                int size1=sc.nextInt();
                int[] arr1=new int[size1];
                System.out.println("elements1");
                
		for(int i=0;i<size1;i++){
                        arr1[i]=sc.nextInt();
                }


                System.out.println("size2");
                int size2=sc.nextInt();
                int[] arr2=new int[size2];
                System.out.println("elements2");


                for(int i=0;i<size2;i++){
                        arr2[i]=sc.nextInt();
                }
		int [] arr3=common_ele(size1,size2,arr1,arr2);
		System.out.println("common ele:");
                for(int i=0;i<size1;i++){
			if(arr3[i]==0){
				continue;
			}
			System.out.print(arr3[i]+" ");
		}
        }
	public static int[] common_ele(int size1,int size2,int[] arr1,int [] arr2){
		int[] arr3=new int[size1];
		for(int i=0;i<size1;i++){
			for(int j=0;j<size2;j++){
				if(arr1[i]==arr2[j]){
					arr3[i]=arr1[i];
				}
			}
		}
	return arr3;
	}
}
					




