/*Que 6 : Second Largest
Given an array Arr of size N, print the second largest distinct element from an array.
Example 1:
Input:
N = 6
Arr[] = {12, 35, 1, 10, 34, 1}
Output: 34
Explanation: The largest element of the array is 35 and the second largest element
is 34.
Example 2:
Input:
N = 3
Arr[] = {10, 5, 10}
Output: 5
Explanation: The largest element of the array is 10 and the second largest element
is 5.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
2 ≤ N ≤ 10
5
1 ≤ Arri ≤ 10
5
*/
import java.util.*;
class demo6{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("size");
		int size=sc.nextInt();
		int[] arr=new int[size];
		System.out.println("elemts");
		for(int i=0;i<size;i++){
			arr[i]=sc.nextInt();
		}
		System.out.println("elements:"+seconds_max(size,arr));
	}
	public static int seconds_max(int size,int[] arr){
			int max=Integer.MIN_VALUE;
			int secmax=0;
			for(int i=0;i<size;i++){
				if(arr[i]>max){
					secmax=max;	
					max=arr[i];
				}
			
			}
			return secmax;
	}
}

