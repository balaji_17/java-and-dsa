/*Que 29 : Key Pair
Given an array Arr of N positive integers and another number X. Determine whether or
not there exist two elements in Arr whose sum is exactly X.
Example 1:
Input:
N = 6, X = 16
Arr[] = {1, 4, 45, 6, 10, 8}
Output: Yes
Explanation: Arr[3] + Arr[4] = 6 + 10 = 16
Example 2:
Input:
N = 5, X = 10
Arr[] = {1, 2, 4, 3, 6}
Output: Yes
Explanation: Arr[2] + Arr[4] = 4 + 6 = 10
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)
Constraints:
1 ≤ N ≤ 10
5
1 ≤ Arr[i] ≤ 10
5
*/

import java.util.*;
import java.util.Arrays;
class target_ele{
        public static void main(String[] args){
                Scanner sc=new Scanner(System.in);

                System.out.println("size");
                int size=sc.nextInt();

                int[] arr=new int[size];
                System.out.println("elements");

                for(int i=0;i<size;i++){
                        arr[i]=sc.nextInt();

                }
		System.out.println("target");
		int X=sc.nextInt();
		System.out.println(target_elemet(size,arr,X));
	}
	public static boolean target_elemet(int size,int[] arr,int X){
		int left=0;
		int right=size-1;
		Arrays.sort(arr);
		while(left<=right){
		int sum=arr[left]+arr[right];
		if(sum==X){
			return true;
		}else if(sum<X){
			left++;
		}else{
			right--;
		}
		}
		return false;
	}
		
}

