/*Que 25 : Even and Odd
Given an array arr[] of size N containing an equal number of odd and even numbers.
Arrange the numbers in such a way that all the even numbers get the even index and odd
numbers get the odd index.
Note: There are multiple possible solutions, Print any one of them. Also, 0-based
indexing is considered.
Example 1:
Input:
N = 6
arr[] = {3, 6, 12, 1, 5, 8}
Output:
1
Explanation:
6 3 12 1 8 5 is a possible solution. The output will always be 1 if your
rearrangement is correct.
Example 2:
Input:
N = 4
arr[] = {1, 2, 3, 4}
Output :
1
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 ≤ N ≤ 10
5
1 ≤ arr[i] ≤ 10
5
*/
import java.util.*;
class evenodd{
        public static void main(String [] a){
                Scanner sc=new Scanner(System.in);
                System.out.println("size");
                int size=sc.nextInt();
                int[] arr=new int[size];

                for(int i=0;i<size;i++){
                        arr[i]=sc.nextInt();
                }

                even_odd(size,arr);
		for(int num:arr){
			System.out.print(num+" ");
		}
        }
        public static int[] even_odd(int size,int [] arr){             
                for(int i=1;i<size;i++){
                        if(arr[i]%2!=0){
                                arr[i]=arr[i];
			}
			if(arr[i]%2==0){
                        	arr[i]=arr[i];
			}

                }
		return arr;


	}
}
