/*Que 2 :Sort an array of 0s, 1s and 2s
Given an array of size N containing only 0s, 1s, and 2s; sort the array in ascending order.
Example 1:
Input:
N = 5
arr[]= {0 2 1 2 0}
Output:
0 0 1 2 2
Explanation: 0s 1s and 2s are segregated into ascending order. */
/*
import java.io.*;

class Demo2{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader (new InputStreamReader(System.in));
		System.out.println("enter size of array");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		System.out.println("enter elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		
		}
	
		sortedarray(arr);
	}
	public static void sortedarray(int [] arr){
		for(int i=0;i<arr.length-1;i++){
			for(int j=0;j<arr.length-1;j++){
				if(arr[j]>arr[j+1]){
					int temp =arr[j];
					arr[j]=arr[j+1];
					arr[j+1]=temp;
				}
			}
		}
		System.out.println("soeted elements");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+"");
		}
	}
}*/

import java.io.*;
class demo2{

     private static boolean fales;

    public static void main(String[] args) throws IOException{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        int size=Integer.parseInt(br.readLine());
        int[] arr=new int[size];

        int n=arr.length;
        boolean swapped;
        do{
            swapped=fales;
            for(int i=1;i<n;i++){
                if(arr[i-1]>arr[i]){
                    arr[i-1]=arr[i-1]+arr[i];
                    arr[i]=arr[i-1]-arr[i];
                    arr[i-1]=arr[i-1]-arr[i];
                    swapped=true;
                    
                }
            }
            n--;
        }while(swapped);
        System.out.println("sorted elements");
        for(int num:arr){
            System.out.println(num+" ");
        }
    }
}				


