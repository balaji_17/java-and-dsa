/*Que 4 : Count pairs with given sum
Given an array of N integers, and an integer K, find the number of pairs of elements in
the array whose sum is equal to K.
Example 1:
Input:
N = 4, K = 6
arr[] = {1, 5, 7, 1}
Output: 2
Explanation:
arr[0] + arr[1] = 1 + 5 = 6
and arr[1] + arr[3] = 5 + 1 = 6.
Example 2:
Input:
N = 4, K = 2
arr[] = {1, 1, 1, 1}
Output: 6
Explanation:
Each 1 will produce sum 2 with any 1.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)
Constraints:
1 <= N <= 10
5
1 <= K <= 10
8
1 <= Arr[i] <= 10
*/
import java.io.*;

class demo4{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter size");
		int size=Integer.parseInt(br.readLine());
			
		int [] arr=new int[size];

		for(int i=0;i<size;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		System.out.println("enter Ksum element");
		int num=Integer.parseInt(br.readLine());
		
		
		System.out.println("elemets are "+count_sum(size,arr,num));

	}
	public static int count_sum(int size,int [] arr,int num){
		int count=0;
		for(int i=0;i<size;i++){
			for(int j=i+1;j<size;j++){
				if(arr[i]+arr[j]==num){
					count++;
				}
			}
		}
		return count;
	}
}




