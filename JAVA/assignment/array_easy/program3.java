/*Que 3 : Find Duplicates in an Array
Given an array of size N which contains elements from 0 to N-1, you need to find all the
elements occurring more than once in the given array. Return the answer in ascending
order. If no such element is found, return list containing [-1].
Note: The extra space is only for the array to be returned. Try and perform all operations
within the provided array.
Example 1:
Input:
N = 4
a[] = {0,3,1,2}
Output:
-1
Explanation:
There is no repeating element in the array. Therefore output is -1.
Example 2:
Input:
N = 5
a[] = {2,3,1,2,3}
Output:
2 3
Explanation:
2 and 3 occur more than once in the given array.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)
Constraints:
1 <= N <= 10
5
0 <= A[i] <= N-1, for each valid i
*/
import java.io.*;
class Demo3{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader (new InputStreamReader(System.in));
		System.out.println("enter size");
		int size=Integer.parseInt(br.readLine());
		int[] arr1=new int[size];	
		
		for(int i=0;i<arr1.length;i++){
			arr1[i]=Integer.parseInt(br.readLine());
		}
		
		int[] arr2=duplicate_ele(size, arr1);
		System.out.println("duplicate elements:");
		
		for (int i = 0; i < arr2.length; i++) {
            		if (arr2[i] != 0) {
                		System.out.println(arr2[i]);
			}
            	}
		
	}
		public static int[] duplicate_ele(int size,int [] arr1){
		int [] arr2=new int [size];
		int count=0;		
			for(int i=0;i<size;i++){
			  	for(int j=i+1;j<size;j++){
					if(arr1[i]==arr1[j]){
						arr2[count++]=arr1[i];
						break;
				}
			}
		}
		return arr2;
	}
	}




