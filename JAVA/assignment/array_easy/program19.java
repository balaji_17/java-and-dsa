/*Que 27 : Remove Duplicate Elements from sorted Array
Given a sorted array A[] of size N, delete all the duplicate elements from A[]. Modify the
array such that if there are X distinct elements in it then the first X positions of the array
should be filled with them in increasing order and return the number of distinct elements
in the array.
Note:
1. Don't use set or HashMap to solve the problem.
2. You must return the number of distinct elements(X) in the array, the driver code will
print all the elements of the modified array from index 0 to X-1 as output of your code.
Example 1:
Input:
N = 5
Array = {2, 2, 2, 2, 2}
Output: 2
Explanation: After removing all the duplicates only one instance of 2 will remain
i.e. {2} so modify array will contain 2 at first position and you should return 1
after modify the array.
Example 2:
Input:
N = 4
Array = {1, 2, 2, 4}
Output: 1 2 4
Explanation: After removing all duplicates, the modified array will contain {1, 2,
4} at first 3 positions so you should return 3 after modifying the array.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 ≤ N ≤ 10
4
1 ≤ A[i] ≤ 10
6
*/

import java.util.*;
class duplicatedemo{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);

		System.out.println("size");
		int size=sc.nextInt();

		int[] arr=new int[size];
		System.out.println("elements");

		for(int i=0;i<size;i++){
			arr[i]=sc.nextInt();

		}
		int x =remove_duplicate(size,arr);
		for(int i=0;i<x;i++){
			System.out.print(arr[i]+" ");
			
		}
	}
	public static int remove_duplicate(int size,int[] arr){
		if(size==0 || size==1){
			return size;
		}
		int j=0;
		for(int i=0;i<size-1;i++){
			if(arr[i]!=arr[i+1]){
					arr[j]=arr[i];
					j++;
				}
			}
		arr[j]=arr[size-1];
		return j;
	}
}

/*  for unsorted array 
   int j=0;
   for(int i=0;i<size-1;i++){
   	boolean isDuplicate=false;
	for(int k=0;k<j;k++){
		if(arr[i]==arr[k]){
			isDupaltacet=true;
			break;
		}
	}
	is(!isDuplicate){
		arr[j]=arr[i];
		j++;
	}
   }
   arr[j]=arr[size-1];
   j++;
   return j;
   }
 }
   */

 

