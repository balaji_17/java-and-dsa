/*Que 10 : First Repeating Element
Given an array arr[] of size n, find the first repeating element. The element should occur
more than once and the index of its first occurrence should be the smallest.
Note:- The piosition you return should be according to 1-based indexing.
Example 1:
Input:
n = 7
arr[] = {1, 5, 3, 4, 3, 5, 6}
Output: 2
Explanation: 5 is appearing twice and its first appearance is at index 2 which is
less than 3 whose first occurring index is 3.
Example 2:
Input:
n = 4
arr[] = {1, 2, 3, 4}
Output: -1
Explanation: All elements appear only once so the answer is -1.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)
Constraints:
1 <= n <= 10
6
0 <= Ai<= 10
6
*/

import java.util.*;
class demo10{
        public static void main(String[] args){
                Scanner sc=new Scanner(System.in);
                System.out.println("size");
                int size=sc.nextInt();
                int [] arr=new int[size];
                System.out.println("elemets");
                for(int i=0;i<size;i++){
                        arr[i]=sc.nextInt();
                }
                System.out.println("First reprating :"+First_repeting(size,arr));
        }
        public static int First_repeting(int size,int[] arr){
		for(int i=0;i<size;i++){
			for(int j=i+1;j<size;j++){
				if(arr[i]==arr[j]){
					return arr[i];
				}
			}
		}
		return -1;
	}
}

