/*Que 15 : Union of two sorted Array
Union of two arrays can be defined as the common and distinct elements in the two
arrays.
Given two sorted arrays of size n and m respectively, find their union.
Example 1:
Input:
n = 5, arr1[] = {1, 2, 3, 4, 5}
m = 3, arr2 [] = {1, 2, 3}
Output: 1 2 3 4 5
Explanation: Distinct elements including both the arrays are: 1 2 3 4 5.
Example 2:
Input:
n = 5, arr1[] = {2, 2, 3, 4, 5}
m = 5, arr2[] = {1, 1, 2, 3, 4}
Output: 1 2 3 4 5
Explanation: Distinct elements including both the arrays are: 1 2 3 4 5.
Example 3:
Input:
n = 5, arr1[] = {1, 1, 1, 1, 1}
m = 5, arr2[] = {2, 2, 2, 2, 2}
Output: 1 2
Explanation: Distinct elements including both the arrays are: 1 2.
Expected Time Complexity: O(n+m).
Expected Auxiliary Space: O(n+m).
Constraints:
1 <= n, m <= 10
5
1 <= arr[i], brr[i] <= 10
6
*/


import java.util.*;
class demo15{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.println("size1");
                int size1=sc.nextInt();
                int[] arr1=new int[size1];
                System.out.println("elements1");

                for(int i=0;i<size1;i++){
                        arr1[i]=sc.nextInt();
                }


                System.out.println("size2");
                int size2=sc.nextInt();
                int[] arr2=new int[size2];
                System.out.println("elements2");


                for(int i=0;i<size2;i++){
                        arr2[i]=sc.nextInt();
                }
                int [] arr3=merge_array(size1,size2,arr1,arr2);
                System.out.println("distant array:");
                for(int i=0;i<size1;i++){
                        if(arr3[i]==0){
                                continue;
                        }
                        System.out.print(arr3[i]+" ");
                }
        }
        public static int[] merge_array(int size1,int size2,int[] arr1,int [] arr2){
                int[] arr3=new int[size1+size2];
		for(int i=0;i<size1;i++){
			for(int j=0;j<size2;j++){
				if(arr1[i]==arr2[j]){
					break;
				}
				arr3[i]=arr1[i];
				arr3[i]=arr2[j];
			}
		}
		return arr3;
	}
}

