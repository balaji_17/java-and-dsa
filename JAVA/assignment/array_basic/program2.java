/*2] Find minimum and maximum element in an array
Given an array A of size N of integers. Your task is to find the minimum and
maximum elements in the array.
Example 1:
Input:
N = 6
A[] = {3, 2, 1, 56, 10000, 167}
Output: 1 10000
Explanation: minimum and maximum elements of array are 1 and 10000.
Example 2:
Input:
N = 5
A[] = {1, 345, 234, 21, 56789}
Output: 1 56789
Explanation: minimum and maximum elements of array are 1 and 56789.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 <= N <= 10^5
1 <= Ai <=10^12
*/

import java.io.*;
class demo2{
        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("enter size");
                int size=Integer.parseInt(br.readLine());

                int[] arr=new int[size];
		//int[] arr1=new int[2];

                System.out.println("enter array ele");
                for(int i=0;i<size;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }          
		
		int[] arr1=min_maxele(size,arr);
		for(int num:arr1){
			System.out.print(num+" ");
		}
             }
        public static int[] min_maxele(int size,int[] arr){
                int[] arr1=new int[2];
		int min=arr[0];
		int max=arr[0];
		for(int i=0;i<size;i++){
                        if(arr[i]>max){
                                max=arr[i];
                        }
			if(arr[i]<min){
				min=arr[i];
			}
                }
		arr1[0]=min;
		arr1[1]=max;
		
		return arr1;
        }

}
