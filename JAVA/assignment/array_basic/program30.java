import java.util.*;

class demo {

    public static int remove_duplicate_elements(int[] arr, int n) {
        if (n == 0 || n == 1)
            return n;
        int[] temp = new int[n];
        int j = 0;
        temp[j++] = arr[0]; // The first element is always unique.

        for (int i = 1; i < n; i++) {
            if (arr[i] != arr[i - 1]) {
                temp[j++] = arr[i];
            }
        }

        for (int i = 0; i < j; i++) {
            arr[i] = temp[i];
        }
        return j;
    }

    public static void main(String[] args) {
        int n;
        Scanner s = new Scanner(System.in);
        System.out.print("Enter no. of elements you want in array:");
        n = s.nextInt();
        int a[] = new int[n];
        System.out.println("Enter all the elements:");

        for (int i = 0; i < n; i++) {
            a[i] = s.nextInt();
        }

        n = remove_duplicate_elements(a, n);
        System.out.print("Array after removing duplicates: ");
        for (int i = 0; i < n; i++) {
            System.out.print(a[i] + " ");
        }
    }
}

