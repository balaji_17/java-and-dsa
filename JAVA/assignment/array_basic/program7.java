/*7] Form largest number from digits
Given an array of numbers from 0 to 9 of size N. Your task is to rearrange elements
of the array such that after combining all the elements of the array, the number
formed is maximum.
Example 1:
Input:
N = 5
A[] = {9, 0, 1, 3, 0}
Output:
93100
Explanation:
Largest number is 93100 which can be formed from array digits.
Example 2:
Input:
N = 3
A[] = {1, 2, 3}
Output:
321
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)
Constraints:
1 <= N <= 10^7
0 <= Ai <= 9
*/

import java.io.*;
import java.util.*;
class demo7{
	public static void main(String [] args)throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("enter size");

		int size=Integer.parseInt(br.readLine());

		int[] arr=new int[size];

		for(int i=0;i<size;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		int[] arr1=formlargestnum(size,arr);

		for(int num:arr1){
			System.out.print(num+" ");
		}
	}
	public static int[] formlargestnum(int size,int[] arr){
		int[] arr1=new int[size];
		Arrays.sort(arr);
		for(int i=size-1,j=0;i>0;i--,j++){
			arr1[j]=arr[i];
		}
		return arr1;
	}
}

