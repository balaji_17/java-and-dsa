/*10] Max Odd Sum
Given an array of integers, check whether there is a subsequence with odd sum and
if yes, then find the maximum odd sum. If no subsequence contains an odd sum,
print -1.
Example 1:
Input:
N=4
arr[] = {4, -3, 3, -5}
Output: 7
Explanation:
The subsequence with maximum odd
sum is 4 + 3 = 7
Example 2:
Input:
N=5
arr[] = {2, 5, -4, 3, -1}
Output: 9
Explanation:
The subsequence with maximum odd
sum is 2 + 5 + 3 + -1 = 9
Expected Time Complexity: O(N).
Expected Auxiliary Space: O(1).
Constraints:
2 ≤ N ≤ 10^7
-10^3 <= arr[i] <= 10^3
*/
import java.util.*;
import java.io.*;
class demo10{
        public static void main(String [] args)throws IOException{
                BufferedReader br=new BufferedReader (new InputStreamReader(System.in));
                System.out.println("size");
                int size=Integer.parseInt(br.readLine());

                int [] arr=new int[size];
                System.out.println("enetr elements");
                for(int i=0;i<size;i++){
                        arr[i]=Integer.parseInt(br.readLine());

                }
		int[] arr1=maxodd_sum(size,arr);
		int n=-1;
		for(int i=0;i<size;i++){
			
			if(arr1[i]>n){
				n=arr1[i];
			}
			}
		System.out.println("sum is: "+n);
            	                   
	}

        public static int[] maxodd_sum(int size,int [] arr){
                int[] arr1=new int[size];
		for(int i=0;i<size;i++){
			//int sum=0;
			for(int j=i+1;j<size;j++){
				int sum=arr[i]+arr[j];	
				if(sum%2!=0 && sum>arr1[i]){
					arr1[i]=sum;	
				}
			
			}			
		}
		return arr1;
                
        }
}
