/*9] Remove an Element at Specific Index from an Array
Given an array of a fixed length. The task is to remove an element at a specific
index from the array.
Examples 1:
Input: arr[] = { 1, 2, 3, 4, 5 }, index = 2
Output: arr[] = { 1, 2, 4, 5 }
Examples 2:
Input: arr[] = { 4, 5, 9, 8, 1 }, index = 3
Output: arr[] = { 4, 5, 9, 1 }
*/
import java.util.*;
import java.io.*;
class demo9{
	public static void main(String [] args)throws IOException{
		BufferedReader br=new BufferedReader (new InputStreamReader(System.in));
		System.out.println("size");
		int size=Integer.parseInt(br.readLine());

		int [] arr=new int[size];
		System.out.println("enetr elements");
		for(int i=0;i<size;i++){
			arr[i]=Integer.parseInt(br.readLine());
		
		}
		
		System.out.println("enetr indexe elements");
		
		int num=Integer.parseInt(br.readLine());
		removeele(size,arr,num);
		for(int i=0;i<size;i++){
			if(arr[i]==0){
				continue;
			}
			System.out.print(arr[i]+" ");
		}
	}
	public static int[] removeele(int size,int [] arr,int num){
		arr[num]=0;
		return arr;
	}
}

