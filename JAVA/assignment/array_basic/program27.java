/*27] Count pair sum
Given two sorted arrays(arr1[] and arr2[]) of size M and N of distinct elements.
Given a value Sum. The problem is to count all pairs from both arrays whose sum
is equal to Sum.
Note: The pair has an element from each array.
Example 1:
Input:
M=4, N=4 , Sum = 10
arr1[] = {1, 3, 5, 7}
arr2[] = {2, 3, 5, 8}
Output: 2
Explanation: The pairs are: (5, 5) and (7, 3).
Example 2:
Input:
N=4, M=4, sum=5
arr1[] = {1, 2, 3, 4}
arr2[] = {5, 6, 7, 8}
Output: 0
Expected Time Complexity: O(M+N).
Expected Auxiliary Space: O(1).
Constraints:
1 ≤ M, N ≤ 10^5
*/
import java.util.*;
import java.io.*;
class demo27{
        public static void main(String [] args)throws IOException{
                BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
                System.out.println("size1");
                int size1=Integer.parseInt(br.readLine());

                int [] arr1=new int[size1];
                System.out.println("elements1");
                for(int i=0;i<size1;i++){
                        arr1[i]=Integer.parseInt(br.readLine());
                }
                System.out.println("size2");
                int size2=Integer.parseInt(br.readLine());

                int [] arr2=new int[size2];
                System.out.println("elements2");
                for(int i=0;i<size2;i++){
                        arr2[i]=Integer.parseInt(br.readLine());
                }
		System.out.println("sum is:");
		int sum=Integer.parseInt(br.readLine());
			

                System.out.println("output:"+sum_2array(size1,size2,arr1,arr2,sum));
        }
	public static int sum_2array(int size1,int size2,int[] arr1,int [] arr2,int sum){
		int count=0;
		int left=0;
		int right=size2-1;

		while(left<size1 && right>=0){
			int currsum=arr1[left]+arr2[right];

			if(currsum==sum){
				count++;
				left++;
				right--;
			}
			else if(currsum<sum){
				left++;
			}
			else{
				right--;
			}
		}
		return count;
	}
}
