/*6] Elements in the Range
Given an array arr[] containing positive elements. A and B are two numbers
defining a range. The task is to check if the array contains all elements in the given
range.
Example 1:
Input: N = 7, A = 2, B = 5
arr[] = {1, 4, 5, 2, 7, 8, 3}
Output: Yes
Explanation: It has elements between range 2-5 i.e 2,3,4,5
Example 2:
Input: N = 7, A = 2, B = 6
arr[] = {1, 4, 5, 2, 7, 8, 3}
Output: No
Explanation: Array does not contain 6.
Note: If the array contains all elements in the given range then driver code outputs
Yes otherwise, it outputs No
Expected Time Complexity: O(N).
Expected Auxiliary Space: O(1).
Constraints:
1 ≤ N ≤ 10^7
*/

import java.io.*;
import java.util.*;

class demo6{
	public static void main(String [] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter size");
		int size=Integer.parseInt(br.readLine());

		int[] arr=new int[size];

		System.out.println("range of A and B");
		int A=Integer.parseInt(br.readLine());
		int B=Integer.parseInt(br.readLine());

		System.out.println("enter elemets");

		for(int i=0;i<size;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		System.out.println(rangeYN(size,arr,A,B));
	}
	public static boolean rangeYN(int size,int []arr,int A,int B){
		Arrays.sort(arr);
		boolean flag=false;
		for(int i=0;i<size;i++){
			if(arr[i]>=A && arr[i]<=B){
				flag =true;
				break;
				
			}
		}
		return flag;
	}
}
				

		


		
