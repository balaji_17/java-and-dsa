/*20] Check if pair with given Sum exists in Array (Two Sum)
Given an array A[] of n numbers and another number x, the task is to check
whether or not there exist two elements in A[] whose sum is exactly x.
Examples:
Input: arr[] = {0, -1, 2, -3, 1}, x= -2
Output: Yes
Explanation: If we calculate the sum of the output,1 + (-3) = -2
Input: arr[] = {1, -2, 1, 0, 5}, x = 0
Output: No
*/

import java.util.*;
import java.io.*;
class demo20{
        public static void main(String [] args)throws IOException{
                BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
                System.out.println("size");
                int size=Integer.parseInt(br.readLine());

                System.out.println("sum");
                int sum=Integer.parseInt(br.readLine());
                int [] arr=new int[size];
                System.out.println("elements");
                for(int i=0;i<size;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }

                System.out.println(pair_sum(size,arr,sum));
        }
	public static boolean pair_sum(int size,int[] arr,int sum){
		for(int i=0;i<size;i++){
			for(int j=0;j<size;j++){
				if(arr[i]+arr[j]==sum && arr[i]!=arr[j]){
					return true;
				}
			}
		}
		return false;
	}
}
