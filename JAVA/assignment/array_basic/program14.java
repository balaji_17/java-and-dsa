/*14] Maximum repeating number
Given an array Arr of size N, the array contains numbers in range from 0 to K-1
where K is a positive integer and K <= N. Find the maximum repeating number in
this array. If there are two or more maximum repeating numbers return the element
having least value.
Example 1:
Input:
N = 4, K = 3
Arr[] = {2, 2, 1, 2}
Output: 2
Explanation: 2 is the most frequent element.
Example 2:
Input:
N = 6, K = 3
Arr[] = {2, 2, 1, 0, 0, 1}
Output: 0
Explanation: 0, 1 and 2 all have the same frequency of 2.But since 0 is
smallest, you need to return 0.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(K)
Constraints:
1 <= N <= 10^7
1 <= K <= N
0 <= Arri <= K - 1
*/
import java.io.*;
class demo13{
        public static void main(String[] args)throws IOException {
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("size1");

                int size1=Integer.parseInt(br.readLine());
                int [] arr1=new int[size1];

                System.out.println("elemets 1");

                for(int i=0;i<size1;i++){
                        arr1[i]=Integer.parseInt(br.readLine());
                }
		System.out.println("k");
                int k=Integer.parseInt(br.readLine());
               
                System.out.println("max repeded ele:"+maxrepeted_occ(size1,arr1,k));
	//maxrepeted_occ(size1,arr1);

        }

        public static int maxrepeted_occ(int size1,int[] arr1,int k){
               	
		for(int i=0;i<size1;i++)
			arr1[(arr1[i]%k)]+=k;
		

                     		int max=arr1[0];
                                int result=0;
			    	for(int i=0;i<size1;i++){
                     		
                            	if(arr1[i]>max){
					max=arr1[i];
					result=i;
				}
			       
                        }
		    
		    
		return result;
		}
	       	


        
        }

