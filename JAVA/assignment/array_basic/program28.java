/*28] Remove Duplicates from unsorted array
Given an array of integers which may or may not contain duplicate elements. Your
task is to remove duplicate elements, if present.
Example 1:
Input:
N = 6
A[] = {1, 2, 3, 1, 4, 2}
Output:
1 2 3 4
Example 2:
Input:
N = 4
A[] = {1, 2, 3, 4}
Output:
1 2 3 4
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)
Constraints:
1<=N<=10^5
1<=A[i]<=10^5
*/
import java.util.*;
class demo28{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("enter size");
		int size=sc.nextInt();
		int [] arr=new int[size];
		System.out.println("enter elemets");
		for(int i=0;i<size;i++){
			arr[i]=sc.nextInt();
		}
		int[] arr1=duplicateEle(size,arr);

		for(int i=0;i<size;i++){
			if(arr[i]==0){
				continue;
			}
			System.out.print(arr[i]+" ");
		}
	}
	public static int[] duplicateEle(int size,int[] arr){
		//int count=0;
		int arr1[]=new int[size];
		for(int i=0;i<size;i++){
			for(int j=i+1;j<size;j++){
				if(arr[i]==arr[j]){
					arr[i]=0;
					}
			}
		}
		//int length=size-count;
		//int arr1[]=new int[length];
		for(int i=0;i<size;i++){
			arr1[i]=arr[i];
		}
		return arr1;

	}
}




