/*19] Find common elements in three sorted arrays
Given three Sorted arrays in non-decreasing order, print all common elements in
these arrays.
Examples:
Input:
ar1[] = {1, 5, 10, 20, 40, 80}
ar2[] = {6, 7, 20, 80, 100}
ar3[] = {3, 4, 15, 20, 30, 70, 80, 120}
Output: 20, 80
Input:
ar1[] = {1, 5, 5}
ar2[] = {3, 4, 5, 5, 10}
ar3[] = {5, 5, 10, 20}
Output: 5, 5
*/

import java.util.*;
import java.io.*;
class demo19{
        public static void main(String [] args)throws IOException{
                BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
                System.out.println("size1");
                int size1=Integer.parseInt(br.readLine());

                int [] arr1=new int[size1];
                System.out.println("elements1");
                for(int i=0;i<size1;i++){
                        arr1[i]=Integer.parseInt(br.readLine());
                }
		System.out.println("size2");
		int size2=Integer.parseInt(br.readLine());

                int [] arr2=new int[size2];
                System.out.println("elements2");
                for(int i=0;i<size2;i++){
                        arr2[i]=Integer.parseInt(br.readLine());
                }
		System.out.println("size3");
		int size3=Integer.parseInt(br.readLine());
                int [] arr3=new int[size3];
                System.out.println("elements3");
                for(int i=0;i<size3;i++){
                        arr3[i]=Integer.parseInt(br.readLine());
                }
                commonele(size1,size2,size3,arr1,arr2,arr3);
        }
	public static void commonele(int size1,int size2,int size3,int[] arr1,int [] arr2,int [] arr3){
		for(int i=0;i<size1;i++){
			for(int j=0;j<size2;j++){
				for(int k=0;k<size3;k++){
					if(arr1[i]==arr2[j] && arr2[j]==arr3[k]){
						System.out.print(arr1[i]+" ");
					}
				}
			}
		}
	}
}






















