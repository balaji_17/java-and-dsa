/*15] Sum of distinct elements
You are given an array Arr of size N. Find the sum of distinct elements in an array.
Example 1:
Input:
N = 5
Arr[] = {1, 2, 3, 4, 5}
Output: 15
Explanation: Distinct elements are 1, 2, 3
4, 5. So the sum is 15.
Example 2:
Input:
N = 5
Arr[] = {5, 5, 5, 5, 5}
Output: 5
Explanation: Only Distinct element is 5.
So the sum is 5.
Expected Time Complexity: O(N*logN)
Expected Auxiliary Space: O(N*logN)
Constraints:
1 ≤ N ≤ 10^7
0 ≤ A[i] ≤ 10^4
*/
import java.util.*;
import java.io.*;
class demo15{
	public static void main(String [] args)throws IOException{
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
		System.out.println("size");
		int size=Integer.parseInt(br.readLine());

		int [] arr=new int[size];
		System.out.println("elements");
		for(int i=0;i<size;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
	
		System.out.println("disnt ele sum:"+distictele_sum(size,arr));
	}
	public static int distictele_sum(int size,int[] arr){
		int count=0;
		int sum=0;

		for(int i=0;i<size;i++){
			boolean flag=true;
			for(int j=0;j<i;j++){
				if(arr[i]==arr[j]){
					flag=false;
					break;
				}
			}
			if(flag){
				arr[count++]=arr[i];
			}
		}
		int[] result=new int[count];
		System.arraycopy(arr,0,result,0,count);

		System.out.println("disnt ele");
		for(int num:result){
			sum=sum+num;
		}
		
		return sum;
		
	}
}
