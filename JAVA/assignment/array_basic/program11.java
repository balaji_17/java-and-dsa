/*11] Product of maximum in first array and minimum in second
Given two arrays of A and B respectively of sizes N1 and N2, the task is to
calculate the product of the maximum element of the first array and minimum
element of the second array.
Example 1:
Input : A[] = {5, 7, 9, 3, 6, 2},
B[] = {1, 2, 6, -1, 0, 9}
Output : -9
Explanation:
The first array is 5 7 9 3 6 2.
The max element among these elements is 9.
The second array is 1 2 6 -1 0 9.
The min element among these elements is -1.
The product of 9 and -1 is 9*-1=-9.
Example 2:
Input : A[] = {0, 0, 0, 0},
B[] = {1, -1, 2}
Output : 0
Expected Time Complexity: O(N + M).
Expected Auxiliary Space: O(1).
Output:
For each test case, output the product of the max element of the first array and the
minimum element of the second array.
Constraints:
1 ≤ N, M ≤ 10^6
*/
import java.io.*;

class demo11{
	public static void main(String[] args)throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("size1");

		int size1=Integer.parseInt(br.readLine());
		int [] arr1=new int[size1];
		
		System.out.println("size2");
		int size2=Integer.parseInt(br.readLine());
		int [] arr2=new int[size1];


		System.out.println("elements 1");

		for(int i=0;i<size1;i++){
			arr1[i]=Integer.parseInt(br.readLine());
		}
		
		System.out.println("elemets 2");

		for(int i=0;i<size2;i++){
			arr2[i]=Integer.parseInt(br.readLine());
		}

		System.out.println("product of min_max is:"+product_min_max(size1,size2,arr1,arr2));


	}

	public static int  product_min_max(int size1,int size2,int[] arr1,int [] arr2){
		int max=arr1[0];
		int min=arr2[0];
		for(int i=0;i<size1;i++){
			if(arr1[i]>max){
				max=arr1[i];
			}
		}
		for(int i=0;i<size2;i++){
			if(arr2[i]<min){
				min=arr2[i];
			}
		}
		int product=max*min;
		return product;
	}
}

