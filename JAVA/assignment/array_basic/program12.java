/*12] First and last occurrences of X
Given a sorted array having N elements, find the indices of the first and last
occurrences of an element X in the given array.
Note: If the number X is not found in the array, return '-1' as an array.
Example 1:
Input:
N = 4 , X = 3
arr[] = { 1, 3, 3, 4 }
Output:
1 2
Explanation:
For the above array, first occurance of X = 3 is at index = 1 and last
occurrence is at index = 2.
Example 2:
Input:
N = 4, X = 5
arr[] = { 1, 2, 3, 4 }
Output:
-1
Explanation:
As 5 is not present in the array, so the answer is -1.
Expected Time Complexity: O(log(N))
Expected Auxiliary Space: O(1)
Constraints:
1 <= N <= 10^5
0 <= arr[i], X <= 10^9
*/

import java.io.*;

class demo12{
        public static void main(String[] args)throws IOException {
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("size1");

                int size1=Integer.parseInt(br.readLine());
                int [] arr1=new int[size1];

 		System.out.println("searched ind_ele");
		int ele=Integer.parseInt(br.readLine());	
                
	        System.out.println("elemets 1");

                for(int i=0;i<size1;i++){
                        arr1[i]=Integer.parseInt(br.readLine());
                }

               	int [] arr2=index_occ(size1,arr1,ele);

	       	for(int i=0;i<size1;i++){
		       if(arr2[i]!=0){
			       System.out.print(arr2[i]+" ");
		       }

        	}
	}

        public static int[] index_occ(int size1,int[] arr1,int ele){
                int[] arr2=new int[size1];
                
                for(int i=0;i<size1;i++){
                        if(arr1[i]==ele){
                                arr2[i]=i;
                        }
                }
                return arr2;
        }
}
