/*23] Find the smallest and second smallest element in an array
Given an array of integers, your task is to find the smallest and second smallest
element in the array. If smallest and second smallest do not exist, print -1.
Example 1:
Input :
5
2 4 3 5 6
Output :
2 3
Explanation:
2 and 3 are respectively the smallest
and second smallest elements in the array.
Example 2:
Input :
6
1 2 1 3 6 7
Output :
1 2
Explanation:
1 and 2 are respectively the smallest
and second smallest elements in the array.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1<=N<=10^5
1<=A[i]<=10^5
*/

import java.util.*;
import java.io.*;
class demo23{
        public static void main(String [] args)throws IOException{
                BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
                System.out.println("size");
                int size=Integer.parseInt(br.readLine());

                int [] arr=new int[size];
                System.out.println("elements");
                for(int i=0;i<size;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }
		smallest_ele(size,arr);
		System.out.print(arr[0]+" "+arr[1]);

               	
        }
	public static int[] smallest_ele(int size,int[] arr){
		boolean swapped;
		do{
			swapped=false;
			for(int i=1;i<size;i++){
				if(arr[i-1]>arr[i]){
					arr[i-1]=arr[i-1]+arr[i];
					arr[i]=arr[i-1]-arr[i];
					arr[i-1]=arr[i-1]-arr[i];
					swapped=true;
				}
			}
			size--;
		}while(swapped);
		return arr;
}
}
