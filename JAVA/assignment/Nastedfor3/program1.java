/*Write a program to print the numbers divisible by 5 from 1 to 50 & the number is even also print the
count of even numbers.
Input: Enter a lower limit: 1
Enter upper limit: 50
Output: 10, 20, 30, 40, 50
Count = 5
*/
import java.io.*;
class demo1{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("enter lower limit");
		int lowerlimit=Integer.parseInt(br.readLine());

		System.out.println("enter upper limit ");
                int upperlimit=Integer.parseInt(br.readLine());
		int count=0;
		for(int i=lowerlimit;i<=upperlimit;i++){
			if(i%2==0){
				
				if(i%5==0){
				System.out.println(""+i);
					count++;
			
				}
			}
		}
		System.out.println("count is= "+count);
	}
}
