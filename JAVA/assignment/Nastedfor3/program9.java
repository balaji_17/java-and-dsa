/*
Q9
write a program to print a series of strong numbers from entered range. ( Take a start and end number
from a user )
Input:-
Enter starting number: 1
Enter ending number: 150
Output:-
Output: strong numbers between 1 and 150
1 2 145 */

import java.io.*;
class demo9{
        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("enter lower limit");
                int lowerlimit=Integer.parseInt(br.readLine());

                System.out.println("enter upper limit ");
                int upperlimit=Integer.parseInt(br.readLine());

                for(int i=lowerlimit;i<=upperlimit;i++){
                        int N=i;
                        int temp=N;
                        int rev=0;
                        int sum=0;
			while(N!=0){
				int rem=N%10;
				int fact=1;
				for(int j=1;j<=rem;j++){
					fact=fact*j;
				}
                              
				N=N/10;
				sum=sum+fact;
                        }
			
                        if(temp==sum){
                                System.out.println("\t"+i);
                        }

                       }

                }
                        }

