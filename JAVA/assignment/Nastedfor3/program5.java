/*Write a program to take a range as input from the user and print perfect squares between that range.
Input: Enter start: 1
Enter end: 100
Output: perfect squares between 1 and 100
*/
import java.io.*;
class demo5{
        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("enter lower limit");
                int lowerlimit=Integer.parseInt(br.readLine());

                System.out.println("enter upper limit ");
                int upperlimit=Integer.parseInt(br.readLine());

                for(int i=lowerlimit;i<=upperlimit;i++){
                        for(int j=1;j<=j*j;j++){
                        if(j*j==i){
                                System.out.println(i+"\t");
                       }

                }
                        }
                }
}

