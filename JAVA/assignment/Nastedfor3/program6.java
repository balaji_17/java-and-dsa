/*Write a program to take 5 numbers as input from the user and print the count of digits in those
numbers.
Input: Enter 5 numbers :
5
The digit count in 5 is 1
25
The digit count in 25 is 2
225
The digit count in 225 is 3
6548
The digit count in 6548 is 4
852347
The digit count in 852347 is 6 */

import java.io.*;
class demo6{
        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("enter 5 numbers:");
		for(int i=1;i<=5;i++){
			
			int numi=Integer.parseInt(br.readLine());
			int count=0;
			int temp=numi;
			while(temp!=0){
				int rem;
				rem=temp%10;
				count++;
				temp=temp/10;
		}

		
		System.out.println("the digit count in"+numi+" is "+count);
                   }

                }
                        }
               

