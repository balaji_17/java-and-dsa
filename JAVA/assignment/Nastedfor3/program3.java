/*Q3
Write a program to take range as input from the user and print perfect numbers.
{Note: Perfect number is the one whose perfect divisor’s addition is equal to that number.
6 is perfect number = 1 + 2 + 3 = 6}
Input: Enter start: 1
Enter end: 30
Output: perfect numbers between 1 and 30
6 28
*/
import java.io.*;
class demo3{
        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("enter lower limit");
                int lowerlimit=Integer.parseInt(br.readLine());

                System.out.println("enter upper limit ");
                int upperlimit=Integer.parseInt(br.readLine());

                for(int i=lowerlimit;i<=upperlimit;i++){
                       	int sum=0;
			int temp=i;
                        for(int j=1;j<=temp/2;j++){
                                if(temp%j==0){
                                        sum=sum+j;
                                }
			}
                        if(temp==sum){
                                System.out.println(""+i);
                        }

                }
                        }
                }
