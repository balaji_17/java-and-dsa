/*Write a program to take range as input from the user and print the palindrome of all numbers. ( Take a
start and end number from a user )
Input: Enter start: 100
Enter end: 200
Output: Palindrome numbers between 100 and 250
*/
import java.io.*;
class demo7{
        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("enter lower limit");
                int lowerlimit=Integer.parseInt(br.readLine());

                System.out.println("enter upper limit ");
                int upperlimit=Integer.parseInt(br.readLine());

                for(int i=lowerlimit;i<=upperlimit;i++){
			int N=i;
			int temp=N;
			int rem;
			int rev=0;
			while(N!=0){
				rem=N%10;
				rev=(rev*10)+rem;
				N=N/10;
			}
			if(temp==rev){
				System.out.println("\t"+i);
			}
                       
                       }

                }
                        }
                


