//factorial
class demo1{
	public static void main(String[] args){
		int x=256946;
		int rev=0;
		while(x!=0){
			int rem=x%10;
			rev=(rev*10)+rem;
			x=x/10;
		}
		while(rev!=0){
			int rem=rev%10;

			if(rem%2==0){
				int fact=1;
				for(int i=1;i<=rem;i++){
					fact=fact*i;
				}
				System.out.print(fact+" ");
			}
			rev=rev/10;
		}
	}
}
