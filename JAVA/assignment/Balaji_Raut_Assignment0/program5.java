class demo5{ 
    public static void main(String[] args) {
        String input = "adgtioseobi";
        int[] vowelCounts = new int[5];

        for (int i = 0; i < input.length(); i++) {
            char ch = input.charAt(i);
            char lowerCh = Character.toLowerCase(ch);

            if (lowerCh == 'a') {
                vowelCounts[0]++;
            } else if (lowerCh == 'e') {
                vowelCounts[1]++;
            } else if (lowerCh == 'i') {
                vowelCounts[2]++;
            } else if (lowerCh == 'o') {
                vowelCounts[3]++;
            } else if (lowerCh == 'u') {
                vowelCounts[4]++;
            }
        }

        System.out.println("Count of 'a': " + vowelCounts[0]);
        System.out.println("Count of 'e': " + vowelCounts[1]);
        System.out.println("Count of 'i': " + vowelCounts[2]);
        System.out.println("Count of 'o': " + vowelCounts[3]);
        System.out.println("Count of 'u': " + vowelCounts[4]);
    }
}

