//WAP to string from user & convert all index of string to uppercase &odd index to lovercase
class demo6 {
    public static void main(String[] args) {
        String input = "dfTbnSrOvryt";
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < input.length(); i++) {
            char ch = input.charAt(i);

            if (i % 2 == 0) {
                
                result.append(Character.toUpperCase(ch));
            } else {
                
                result.append(Character.toLowerCase(ch));
            }
        }

        System.out.println("Original String: " + input);
        System.out.println("Modified String: " + result.toString());
    }
}

