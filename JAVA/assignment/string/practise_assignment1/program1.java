import java.io.*;
class demo1{
	
	 void printarr(int arr[][]){
		for(int i=0;i<arr.length;i++){
                        for(int j=0;j<arr[i].length;j++){
                                if(i!=j){
					System.out.print(arr[i][j] +" ");
				}
                        }
			System.out.println();
                }
	}

	public static void main(String [] args) throws IOException{
		BufferedReader br=new BufferedReader (new InputStreamReader(System.in));

		System.out.println("enter rows");
		int row=Integer.parseInt(br.readLine());

		System.out.println("enter cols");
                int col=Integer.parseInt(br.readLine());

		int arr[][]=new int[row][col];

		System.out.println("enter element into the 2-d array");

		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				arr[i][j]=Integer.parseInt(br.readLine());
			}
		}

		demo1 obj=new demo1();

		obj.printarr(arr);
	}
}

