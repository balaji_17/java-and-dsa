// Static inner class 
class outer {
	void m1(){
		System.out.println("in outer m1");
	}
	static class inner{
		void m1(){
			System.out.println("in inner m1");
		}
	}
}
class client{
	public static void main(String[] args){
		outer.inner obj=new outer.inner();
		obj.m1();
	}
}

