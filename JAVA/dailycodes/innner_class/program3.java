// method local inner class
class Outer{
	void m1(){
		System.out.println("in outer m1");
		class Inner{
			void m1(){
				System.out.println("in inner m1");
			}
		}

		Inner obj=new Inner();
		obj.m1();
	}
	void m2(){
		System.out.println("in outer m2");
	}
	public static void main(String[] args){
		Outer obj1= new Outer();
		obj1.m1();// here we have to call m1 method so inner class is aslo called.
		obj1.m2();
	}
}


