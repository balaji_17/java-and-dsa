class SecondLargestDistinctElement {
    public static int findSecondLargestDistinctElement(int[] arr) {
        int n = arr.length;
        
       
        int largest = arr[0];
        int secondLargest = arr[0];
        
        
        for (int i = 0; i < n; i++) {
            if (arr[i] > largest) {
                secondLargest = largest;
                largest = arr[i];
            } else if (arr[i] > secondLargest && arr[i] != largest) {
                secondLargest = arr[i];
            }
        }
        
       
        return secondLargest;
    }
    
    public static void main(String[] args) {
        int[] arr = {12, 35, 1, 10, 355555,33};
        int secondLargest = findSecondLargestDistinctElement(arr);
        System.out.println("Second largest distinct element: " + secondLargest);
    }
}

