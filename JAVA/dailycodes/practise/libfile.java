import java.util.Scanner;

class Book {
    private int accessionNumber;
    private String author;
    private String title;
    private boolean isIssued;

    public Book(int accessionNumber, String author, String title, boolean isIssued) {
        this.accessionNumber = accessionNumber;
        this.author = author;
        this.title = title;
        this.isIssued = isIssued;
    }

    public int getAccessionNumber() {
        return accessionNumber;
    }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public boolean isIssued() {
        return isIssued;
    }

    public void setIssued(boolean issued) {
        isIssued = issued;
    }
}

class Library {
    private Book[] books;
    private int totalBooks;

    public Library(int capacity) {
        books = new Book[capacity];
        totalBooks = 0;
    }

    public void addBook(Book book) {
        books[totalBooks] = book;
        totalBooks++;
    }

    public void displayBookInfo() {
        System.out.println("Book Information:");
        System.out.println("Accession No.\tAuthor\t\tTitle\t\tIssued");

        for (int i = 0; i < totalBooks; i++) {
            Book book = books[i];
            System.out.println(book.getAccessionNumber() + "\t\t" + book.getAuthor() + "\t\t" + book.getTitle() + "\t\t" + book.isIssued());
        }
    }

    public void displayBooksByAuthor(String author) {
        System.out.println("Books by author " + author + ":");
        System.out.println("Accession No.\tAuthor\t\tTitle\t\tIssued");

        for (int i = 0; i < totalBooks; i++) {
            Book book = books[i];
            if (book.getAuthor().equalsIgnoreCase(author)) {
                System.out.println(book.getAccessionNumber() + "\t\t" + book.getAuthor() + "\t\t" + book.getTitle() + "\t\t" + book.isIssued());
            }
        }
    }

    public void displayNumberOfBooksByTitle(String title) {
        int count = 0;
        for (int i = 0; i < totalBooks; i++) {
            Book book = books[i];
            if (book.getTitle().equalsIgnoreCase(title)) {
                count++;
            }
        }
        System.out.println("Number of books with title '" + title + "': " + count);
    }

    public void displayTotalNumberOfBooks() {
        System.out.println("Total number of books in the library: " + totalBooks);
    }
}

class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Create a library with a maximum capacity of 100 books
        Library library = new Library(100);

        // Add books to the library
        library.addBook(new Book(101, "Author1", "Book1", false));
        library.addBook(new Book(102, "Author2", "Book2", true));
        library.addBook(new Book(103, "Author1", "Book3", true));
        library.addBook(new Book(104, "Author3", "Book4", false));
        library.addBook(new Book(105, "Author2", "Book5", true));

        int choice;
        do {
            System.out.println("\n---- Library Management System ----");
            System.out.println("1 - Display book information");
            System.out.println("2 - Display all books by a particular author");
            System.out.println("3 - Display the number of books of a particular title");
            System.out.println("4 - Display the total number of books in the library");
            System.out.println("0 - Exit");
            System.out.print("Enter your choice: ");
            choice = scanner.nextInt();

            switch (choice) {
                case 1:
                    library.displayBookInfo();
                    break;
                case 2:
                    System.out.print("Enter the author's name: ");
                    String author = scanner.next();
                    library.displayBooksByAuthor(author);
                    break;
                case 3:
                    System.out.print("Enter the book title: ");
                    String title = scanner.next();
                    library.displayNumberOfBooksByTitle(title);
                    break;
                case 4:
                    library.displayTotalNumberOfBooks();
                    break;
                case 0:
                    System.out.println("Exiting the Library Management System. Goodbye!");
                    break;
                default:
                    System.out.println("Invalid choice. Please try again.");
            }
        } while (choice != 0);

        scanner.close();
    }
}

