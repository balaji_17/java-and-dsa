import java.util.Scanner;
class DataOverflowException extends RuntimeException{
	DataOverflowException(String msg){
		super(msg);
	}
}

class DataUnderflowException extends RuntimeException{
        DataUnderflowException(String msg){
                super(msg);
        }
}

class ArrayDemo{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);

		System.out.println("enter integer value");
		System.out.println("note: 0<integer100");
		
		int arr[] =new int[5];
		for(int i=0;i<arr.length;i++){
			int data=sc.nextInt();
		
		if(data<0)
			throw new DataUnderflowException("less than 0");
		if(data>100)
			throw new  DataOverflowException("more than 100");
	
		arr[i]=data;
		}	
		for(int i=0;i<arr.length;i++){
			System.out.println(arr[i]+" ");
		}
	}
}

	
