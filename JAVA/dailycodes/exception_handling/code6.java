import java.util.Scanner;

class weatherException extends RuntimeException{

	 weatherException(String msg){
		 
		 super(msg);
	 
	 }
}
class weatherDemo{
	public static void main(String[] args){
		Scanner sc= new Scanner(System.in);
		System.out.println("enter rain in mm");
		int rain=sc.nextInt();
		if(rain>5000){
			throw new weatherException("its red alert");
		}
		else {
			throw new weatherException("normal rail-fall");
	}
}
}
