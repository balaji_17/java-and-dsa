class Demo{
        public static void main(String[] args){
                int x=5;
                int y=7;
		int ans1=++x + x++;
		int ans2=++x + --x + --y + --y;

                System.out.println(ans1);//12
                System.out.println(ans2);//26
                System.out.println(x);//7
                System.out.println(y);//5
        }
}
