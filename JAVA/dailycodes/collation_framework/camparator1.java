import java.util.*;
class Laptop{
       String lName=null;
       double price=0.0;

       Laptop(String lName,int price){
	       this.lName=lName;
	       this.price=price;

       }

       public String toString(){
	       return "{"+lName+":"+price+"}";
       }
}

class sortByname implements Comparator{
	public int compare(Object obj1,Object obj2){
		return ((Laptop)obj1).lName.compareTo(((Laptop)obj2).lName);
	}
}
class sortByprice implements Comparator{
        public int compare(Object obj1,Object obj2){
                return (int)(((Laptop)obj1).price - ((Laptop)obj2).price);
        }
}

class Laptopdemo{
	public static void main(String[] args){
		ArrayList al = new ArrayList();
		al.add(new Laptop("DELL",85000));
		al.add(new Laptop("HP",95000));
		al.add(new Laptop("ASUS",45000));
		al.add(new Laptop("MI",65000));
	
	
		System.out.println(al);

		Collections.sort(al,new sortByname());
		System.out.println(al);
		
		Collections.sort(al,new sortByprice());
		System.out.println(al);
	}
}

