// using Camparator
import java.util.*;
class Platform{
	String str=null;
	int foundyear=0;

	Platform(String str,int foundyear){
		this.str=str;
		this.foundyear=foundyear;

	}

	public String toString(){
		return "{" + str + ":" + foundyear + "}";

	}

}

class SortByName implements Comparator{

	public int compare(Object obj1,Object obj2){
		return ((Platform)obj1).str.compareTo(((Platform)obj2).str);
	}
}

class TreeMapDemo{
	public static void main(String[] args){
		TreeMap tm=new TreeMap(new SortByName());
		
/*	       tm.put(new Platform("youtube" ,2005),"google");
               tm.put(new Platform("instagram", 2016),"mata");
               tm.put(new Platform("facebook", 2008),"meta");
               tm.put(new Platform("chatgpt", 2022),"openAI");
*/
	       tm.put(new Platform("Balaji",2002) ,"google");
               tm.put(new Platform("Mahesh",2003),"mata");
               tm.put(new Platform("Kunal",2000),"meta");
               tm.put(new Platform("Yash",2100),"microsoft");

               System.out.println(tm);
	}
}	

