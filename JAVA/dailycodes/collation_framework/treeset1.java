import java.util.*;

class Movies implements Comparable{
	String movieName=null;
	double totalcoll=0.0;

	Movies(String movieName,double totalcoll){
		this.movieName = movieName;
		this.totalcoll = totalcoll;
	}

	public int compareTo(Object obj){
		
		return (movieName.compareTo(((Movies)obj).movieName));
	}

	public String toString(){
		return movieName;
	}
}

class TreeSetDemo{
	public static void main(String[] args){
		TreeSet ts = new TreeSet();
		ts.add(new Movies("peacky blindes",1000));
		ts.add(new Movies("money heist",2000));
		ts.add(new Movies("GOT",5000));
		ts.add(new Movies("breaking bads",7000));

		System.out.println(ts);
	}
}
