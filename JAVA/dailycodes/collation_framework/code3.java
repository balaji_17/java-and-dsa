import java.util.*;

class LinedlistDemo{
	public static void main(String[] args){
		LinkedList ll =new LinkedList();

		ll.add(20);
		ll.addFirst(10);
		ll.addLast(30);

		System.out.println(ll);

		ll.add(25);

		System.out.println(ll);
	
		System.out.println("for each loop");
		for(var data:ll){
			System.out.println(data);
		}

	}
}
