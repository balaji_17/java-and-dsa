import java.util.*;

class SortedMapdemo{
	public static void main(String[] args){
		SortedMap tm = new TreeMap();
		 tm.put("ind","india");
                 tm.put("pak","pakistan");
                 tm.put("ban","bangladesh");
                 tm.put("aus","Australiya");

		 System.out.println(tm);

		 System.out.println(tm.subMap("ind","pak"));

		  System.out.println(tm.headMap("pak"));
		   System.out.println(tm.tailMap("pak"));
		   System.out.println(tm.firstKey());
		   System.out.println(tm.lastKey());
		   System.out.println(tm.keySet());
		   System.out.println(tm.values());
	}
}
