// using Comparable

import java.util.*;

class Platform implements Comparable{
	String str=null;
	int foundyear=0;

	Platform(String str,int foundyear){
		this.str=str;
		this.foundyear=foundyear;

	}

	public String toString(){
		return "{"+str + ":" + foundyear +"}";
	}

	public int compareTo(Object obj){

		//return this.foundyear - ((Platform)obj).foundyear;
		return this.str.compareTo(((Platform)obj).str);
	}
}

class TreeMetDemo{
	public static void main(String[] args){
	       TreeMap tm= new TreeMap();

	       tm.put(new Platform("youtube" ,2005),"google");
	       tm.put(new Platform("instagram", 2016),"mata");
	       tm.put(new Platform("facebook", 2008),"meta");
       	       tm.put(new Platform("chatgpt", 2022),"openAI");
	       
	       System.out.println(tm);
	}
}


