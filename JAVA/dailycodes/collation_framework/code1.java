import java.util.*;

class Arraylistdemo{
	public static void main(String[] args){
		ArrayList al = new ArrayList();

		al.add(10);
		al.add(20);
		al.add(20.5f);
		al.add("balaji");

		System.out.println(al);
		
		//add(int , Element)
		al.add(3,"java");
		System.out.println(al);

		//size
		System.out.println(al.size());

		//contain(object)
		 System.out.println(al.contains("BNR"));  //false
		

		//index(object)
		 System.out.println(al.indexOf(10));


		//lastindex(object)
		 System.out.println(al.lastIndexOf(10));

		//get(int)
		 System.out.println(al.get(3));

		 //set(index,raut)
		 System.out.println(al.set(3,"raut"));
		 System.out.println(al);

		 //addall(collaction)

		ArrayList al2 = new ArrayList();

		al2.add("python");
		al2.add("flutter");

		addAll(al2);
	}
}
