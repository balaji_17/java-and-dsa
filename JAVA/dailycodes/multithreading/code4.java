import java.util.Scanner;
class mypriorityException extends RuntimeException{
	mypriorityException(String msg){
	super(msg);
}
}

class mythread extends Thread{
	public void run(){
		Thread t=Thread.currentThread();
		System.out.println(t.getPriority());
	}
}
class ThreadDemo{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
	
		Thread t=Thread.currentThread();
		System.out.println("main thread:" + t.getPriority());
			
		mythread obj1=new mythread();
		obj1.start();
		
		System.out.println("enter set priority");
		int num=sc.nextInt();

		if(num>10){
			throw new mypriorityException("values show be less than 10");
		}
		if(num<1){
			 throw new mypriorityException("values show be more than 1");
                }

		t.setPriority(num);
		
	
		
		mythread obj2=new mythread();
                obj2.start();
	}
}
