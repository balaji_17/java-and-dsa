class MyThread extends Thread{
	MyThread(ThreadGroup tg,String str){
		super(tg,str);
	}

	public void run(){
		System.out.println(Thread.currentThread());
	}
}

class threadgroupdemo{
	public static void main(String[] args){
		ThreadGroup pthread = new ThreadGroup("core2web");

		MyThread obj1 = new MyThread(pthread,"C");
		MyThread obj2 = new MyThread(pthread,"java");
		MyThread obj3 = new MyThread(pthread,"python");

		obj1.start();
		obj2.start();
		obj3.start();

		ThreadGroup cthread = new ThreadGroup(pthread,"incuebater");

		MyThread obj4 = new MyThread(cthread,"flutter");
                MyThread obj5 = new MyThread(cthread,"reactjs");
                MyThread obj6 = new MyThread(cthread,"boot");

		obj4.start();
                obj5.start();
                obj6.start();

	}
}

