class TEST extends Thread{
	TEST(ThreadGroup tg,String str){
		super(tg,str);

	}
	public void run(){
		System.out.println("5-days");
		System.out.println("red ball");
	}
}

class ODI extends Thread{
	ODI(ThreadGroup tg,String str){
		super(tg,str);
	}
	public void run(){
		System.out.println("50-overs");
		System.out.println("white ball");
	}
}

class T20 extends Thread{
	T20(ThreadGroup tg,String str){
		super(tg,str);
	}
	public void run(){
		System.out.println("20-overs");
		System.out.println("white ball");
	}
}

class ThreadgroupCricket{
	public static void main(String[] args){
		ThreadGroup cricket = new ThreadGroup("ICC");

		ThreadGroup test = new ThreadGroup(cricket,"TEST CRICKET");
		TEST formt1 =new TEST(test,"test cricket");

		ThreadGroup odi = new ThreadGroup(cricket,"ODI CRICKET");
                ODI formt2 = new ODI(odi,"odi cricket");

		ThreadGroup t20 = new ThreadGroup(cricket,"T20 CRICKET");
                T20 formt3 = new T20(t20,"t20 cricket");

		formt1.start();
		formt2.start();
		formt3.start();

	}
}


