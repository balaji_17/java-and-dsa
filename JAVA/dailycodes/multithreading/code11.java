import java.util.*;

class mythread extends Thread{
	mythread(ThreadGroup tg,String str){
		super(tg,str);
	}
	public void run(){
		System.out.println(Thread.currentThread());

		try{
			Thread.sleep(3000);
		}
		catch(InterruptedException ie){
			System.out.println(ie.toString());

		}
	}
}

class ThreadDemo{
	public static void main(String[] args){

		ThreadGroup pThreadGP = new ThreadGroup("INDIA");

		mythread t1 = new mythread(pThreadGP,"maha");
		mythread t2 = new mythread(pThreadGP,"goa");

		t1.start();
		t2.start();

		ThreadGroup cThreadGP = new ThreadGroup(pThreadGP,"pakistan");

		mythread t3 = new mythread(cThreadGP,"karachi");
		mythread t4 = new mythread(cThreadGP,"lahor");

		t3.start();
                t4.start();

		cThreadGP.interrupt();

		System.out.println(pThreadGP.activeCount());
		System.out.println(pThreadGP.activeGroupCount());
	}
}
		

