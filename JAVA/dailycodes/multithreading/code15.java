// thread group using runnableinterface

class myThread implements Runnable{
	public void run(){
		System.out.println(Thread.currentThread());
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
			System.out.println(ie.toString());
		}
	}
}

class ThreadGroupDemo{
	public static void main(String[] args){
		ThreadGroup pThread = new ThreadGroup("INDIA");

		myThread obj1 = new myThread();
		myThread obj2 = new myThread();

		Thread t1 = new Thread(pThread,obj1,"MAHA");
		Thread t2 = new Thread(pThread,obj2,"GOA");

		t1.start();
		t2.start();

	}
}



