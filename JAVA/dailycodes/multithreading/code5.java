class mythread extends Thread{
	public void run(){
		System.out.println(Thread.currentThread());
	}
}

class threadDemo{
	public static void main(String[] args)throws InterruptedException{
		System.out.println(Thread.currentThread());

		mythread obj = new mythread();
		obj.start();

		Thread.sleep(1000);

		Thread.currentThread().setName("core2web");

		System.out.println(Thread.currentThread());
	}
}
