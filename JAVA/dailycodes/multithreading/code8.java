class Mythread extends Thread{
	Mythread(ThreadGroup tg,String str){

		super(tg,str);

	}
	
	public void run(){
		System.out.println(Thread.currentThread());
	}
}
class ThreadgroupDemo{
	public static void main(String[] args){
		
		ThreadGroup pthread = new ThreadGroup("Core2web");
		

		Mythread obj1 = new Mythread(pthread,"C");
		Mythread obj2 = new Mythread(pthread,"python");
		Mythread obj3 = new Mythread(pthread,"java");

		obj1.start();
		obj2.start();
		obj3.start();

	}
}
