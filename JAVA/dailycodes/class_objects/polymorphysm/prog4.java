class parent{
        parent(){
                System.out.println("in parent const");
        }

        void fun(int x){
                System.out.println("in parent fun");
        }
}

class child extends parent{
        child(){
                 System.out.println("in child const");

        }

        void fun(){
                 System.out.println("in child fun");

        }
}


class client{
        public static void main(String[] args){
                parent obj1=new child();

                obj1.fun();
        }
}
