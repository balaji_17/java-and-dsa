class demo{
	int x=10;
	static int y=20;

	static{
		System.out.println("static block 1");
	}
	public static void main(String[] arhs){
		System.out.println("in main");
		demo obj=new demo();
		System.out.println(obj.x);
	}
	static {
		System.out.println("int static block 2");
		System.out.println(y);
	}
}
