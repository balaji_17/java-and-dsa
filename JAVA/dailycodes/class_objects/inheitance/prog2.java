class parent{
	int x=10;
	static int y=20;

	static{
		System.out.println("in static block 1");
	}

	parent(){
		System.out.println("in parent construtor block ");
	}

	void methodOne(){
		System.out.println(x);
		System.out.println(y);

	}

	static void methodTwo(){
		System.out.println(y);
	}
};

class child extends parent{
	static{
		System.out.println("in static block 2");
	}

	child(){
		System.out.println("in child construtcor block ");
	}
};


class client{
	public static void main(String[] args){
		child obj1=new child();
		obj1.methodOne();
		obj1.methodTwo();
	}
}
