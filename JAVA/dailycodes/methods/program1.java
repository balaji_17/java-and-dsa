class methoddemo{
		void fun(){
			System.out.println("in fun method");
		}

		static void gun(){
			System.out.println("in gun method");
		}

		public static void main(String[] args){
			gun();
			/*fun(); (error): non-static method fun() cannot be referenced from a static context.
			we have to create object so that we can access non static methos */

			methoddemo obj=new methoddemo();
			obj.fun();
		}
}

