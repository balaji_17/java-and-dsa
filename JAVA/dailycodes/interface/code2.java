interface demo1{
	void fun();
}

interface demo2{
	void fun();

}

class demochild implements demo1,demo2{
	public void fun(){
		System.out.println("in fun");
	}
}
class Client{
	public static void main(String[] args){
		demo1 obj=new demochild();
		obj.fun();
	}
}

