/*Que 5: WAP to toggle the String to uppercase or lowercase
Input: Java output: jAVA
Input: data output: DATA*/

import java.util.*;
class demo5{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);

		String str=sc.next();
		String result=upper_lowercase(str);
		System.out.println("new String: " + result);

	}
	public static String upper_lowercase(String str){
		StringBuilder result = new StringBuilder();
		
		for(int i=0;i<str.length();i++){
			char c=str.charAt(i);
			if(c>='A'&& c<='Z'){
				result.append((char)(c+32));
			}else if(c>='a'&& c <='z'){
				result.append((char)(c-32));
			}
			else{
				result.append(c);
			}
		}
			return result.toString();
		}
	}

