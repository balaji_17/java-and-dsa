/*Que 2: WAP to print the following pattern
Take row input from the user
A
B A
C B A
D C B A */

class demo2{
	public static void main(String[] args){
		int ch=64;
		for(int i=1;i<=4;i++){
			ch=ch+i;
			for(int j=1;j<=i;j++){
				System.out.print((char)ch+" ");
				ch--;
			}
			System.out.println();
		}
	}
}
