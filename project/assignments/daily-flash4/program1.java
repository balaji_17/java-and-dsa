/*Que 1: WAP to print the following pattern
Take input from the user
A B C D
# # # #
A B C D
# # # #
A B C D */

class demo1{
	public static void main(String[] args){
		char ch='A';
		char ch2='#';

		for(int i=1;i<=5;i++){
			if(i%2!=0){
				ch='A';
			}
			for(int j=1;j<=4;j++){
				if(i%2!=0){
					System.out.print(ch+" ");
					ch++;
				}
				else{
					System.out.print(ch2+" ");
				}
			}
			System.out.println();
		}
	}
}

