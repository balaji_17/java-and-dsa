/*Que 4 : WAP to print the composite numbers in the given range
Input: start:1
end:100*/
import java.util.*;
class demo4{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("enter start and end");

		int start=sc.nextInt();
		int end=sc.nextInt();

		range_composite(start,end);
	}
	public static void range_composite(int start,int end){
		for(int i=start;i<=end;i++){
			int count=0;
			for(int j=1;j<=i;j++){
				if(i%j==0){
					count++;
				}
			}
			if(count!=2){
				System.out.println(i);
			}
		}
	}
}


