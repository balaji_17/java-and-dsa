/*Que 5 : WAP to check whether the string contains vowels and return
the count of vowels.*/
import java.util.*;
class demo5{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("enter string");
		String str=sc.next();

		char[] arr=str.toCharArray();
		System.out.println("count is: "+string_vowels(arr));
	}
	public static int string_vowels(char[] arr){
		int count=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]=='a'|| arr[i]=='e'|| arr[i]=='i'||arr[i]=='o'||arr[i]=='u'){
				count++;
			}
		}
		return count;
	}

}
