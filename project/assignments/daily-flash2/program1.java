/*Que 1 : WAP to print the following pattern
Take input from user
A B C D
B C D E
C D E F
D E F G*/

class demo1{
	public static void main(String[] args){
		int ch=65;
		for(int i=0;i<4;i++){
			ch=65+i;	
			for(int j=1;j<=4;j++){
				System.out.print((char)ch+" ");
				ch++;
			}
			System.out.println();
		}
	}
}
