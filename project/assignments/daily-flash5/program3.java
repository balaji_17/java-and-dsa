//Que 3: WAP to check whether the given number is a strong number or not.
import java.util.*;

class demo3{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("number");

		int num=sc.nextInt();

		strongNumber(num);
	}
	public static void strongNumber(int num){
		int temp=num;
		int sum=0;
		while(num!=0){
			int rem=num%10;
			int fact=1;
			for(int i=1;i<=rem;i++){
				fact=fact*i;
			}
			sum=sum+fact;
			num=num/10;
		}
		if(sum==temp){
			System.out.println("strong");
		}else{
			System.out.println("not strong");
		}
		
	}
}
