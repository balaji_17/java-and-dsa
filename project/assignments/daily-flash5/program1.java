/*Que 1: WAP to print the factorial of digits in a given range.
Input: 1-10*/
import java.util.*;
class demo1{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("enter range A and B");

		int start=sc.nextInt();
		int end=sc.nextInt();

		factorial(start,end);
	}
	public static void factorial(int start,int end){

		for(int i=start;i<=end;i++){
			int fact=1;
			int temp=i;
			for(int j=1;j<=temp;j++){
				fact=fact*j;
			}
			System.out.println(temp+" ="+fact);
		}
	}
}

