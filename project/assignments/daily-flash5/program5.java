/*Que 5: WAP to print the occurrence of a letter in given String.
Input String: “Know the code till the core”
Alphabet : o
Output: 3*/
import java.util.*;
class demo5{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		String str=sc.next();

		char target='o';

		int num=position_letter(str,target);

		System.out.println("occ="+num);
	}
	public static int position_letter(String str,char target){

		for(int i=1;i<=str.length();i++){
			if(str.charAt(i)==target){
				return i+1;
			}
		}
		return -1;
	}
}
