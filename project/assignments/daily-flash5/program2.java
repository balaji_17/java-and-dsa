/*Que 2: WAP to print the following pattern
Take row input from the user
a
A B
a b c
A B C D */
import java.util.*;

class demo2{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);

		int row=sc.nextInt();

		pattern(row);
	}
	public static void pattern(int row){
		//char ch1='a';
		//char ch2='A';

		for(int i=1;i<=row;i++){
			char ch1='a';
                	char ch2='A';

			for(int j=1;j<=i;j++){
				if(i%2==0){
					System.out.print(ch2+" ");
					ch2++;
				}else{
					System.out.print(ch1+" ");
					ch1++;
				}
			}
			System.out.println();
		}
	}
}

