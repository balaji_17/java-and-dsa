//Que 3 : WAP to check whether the given no is a palindrome number or not.
//
import java.util.*;
class demo3{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);

		int num=sc.nextInt();
		palindrome(num);
	}
	public static void palindrome(int num){
		int temp=num;
		int rev=0;
		while(num!=0){
			int rem=num%10;
			rev=(rev*10)+rem;
			num=num/10;
		}
		if(temp==rev){
			System.out.println("yas");
		}else{
			System.out.println("no");
		}
	}
}
