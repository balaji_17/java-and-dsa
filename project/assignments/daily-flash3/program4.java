/*Que 4 : WAP to print each reverse numbers in the given range
Input: start:25435
end: 25449  */

import java.util.*;

class demo4{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		int start=sc.nextInt();
		int end=sc.nextInt();

		reverseNum(start,end);
	}
	public static void reverseNum(int start,int end){
		for(int i=start;i<=end;i++){
			int temp=i;
			int rev=0;
			while(temp!=0){
				int rem=temp%10;
				rev=(rev*10)+rem;
				temp=temp/10;
			}
			System.out.println(rev+" ");
		}
	}
}


