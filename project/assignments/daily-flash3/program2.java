/*Que 2 : WAP to print the following pattern
Take row input from user
1
2 1
3 2 1
4 3 2 1*/

import java.util.*;
class demo2{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);

		int row=sc.nextInt();
		int num;
		for(int i=1;i<=row;i++){
			num=i;
			for(int j=1;j<=i;j++){
				System.out.print(num+" ");
				num--;
			}
			System.out.println();
		}
	}
}
