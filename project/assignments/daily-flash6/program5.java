/*Que 5: WAP to replace vowels to # in given string
Input:Meta Data
output: M#t# D#t#*/

import java.util.Scanner;
class demo5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a string:");
        String input = sc.nextLine();

        String result = replaceVowal(input);
        System.out.println("Output: " + result);
    }

    public static String replaceVowal(String input) {
        StringBuilder output = new StringBuilder();

        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (isVowel(c)) {
                output.append('#');
            } else {
                output.append(c);
            }
        }

        return output.toString();
    }

    public static boolean isVowel(char c) {
        c = Character.toLowerCase(c);
        return c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u';
    }
}

