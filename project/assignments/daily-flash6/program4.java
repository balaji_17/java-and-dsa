/*Que 4: WAP to print strong numbers in a given range.
Input: 1 to 10*/
import java.util.*;
class demo4{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("start and end");
		int start=sc.nextInt();
		int end=sc.nextInt();

		strong_num(start,end);
	}
	public static void strong_num(int start,int end){
		for(int i=start;i<=end;i++){
			int temp=i;
			int sum=0;
			int num=i;
			while(num!=0){
				int rem=num%10;
				int fact=1;
				for(int j=1;j<=rem;j++){
					fact=fact*j;
				}
				sum=sum+fact;
				num=num/10;
			}
				if(temp==sum){
					System.out.println(temp+" ");
				}
			}
		}
	}

