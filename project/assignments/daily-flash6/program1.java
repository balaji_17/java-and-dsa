/*Que 1: WAP to print the following pattern
Take input from the user
A B C D
1 3 5 7
A B C D
9 11 13 15
A B C D*/

import java.util.*;

class demo1{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("rows");
		int row=sc.nextInt();
		System.out.println("num");
		int num=sc.nextInt();
		pattern(num,row);
	}
	public static void pattern(int num,int row){
		for(int i=1;i<=row;i++){
			char ch='A';
			for(int j=1;j<=4;j++){
				if(i%2==0){
					System.out.print(num+" ");
					num+=2;
				}else{
					System.out.print(ch+" ");
					ch++;
				}
			}
			System.out.println();
		}
	}
}
